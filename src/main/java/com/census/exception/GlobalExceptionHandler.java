package com.census.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.census.dto.ResponseDto;
import com.census.utils.Constants;

/**
 * 
 * @author mahesh.jammula
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

  /**
   * Globle excpetion handler response entity.
   * @param ex the ex
   * @param request the request
   * @return the response entity
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<?> globleExcpetionHandler(Exception ex, WebRequest request) {
		ResponseDto response = new ResponseDto(Constants.FAIL, null, "Unable to process request. Please try again later!", null);
//    ErrorResponse errorDetails =
//        new ErrorResponse(new Date(), HttpStatus.INTERNAL_SERVER_ERROR.toString() ,ex.getMessage(), request.getDescription(false));
    return new ResponseEntity<>(response, HttpStatus.OK);
  }
}
