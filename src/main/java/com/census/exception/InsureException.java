package com.census.exception;

/**
 * 
 * @author mahesh.jammula
 *
 */
public class InsureException extends Exception {

	private static final long serialVersionUID = 6774413491484487103L;
	
	private final String message;

	/**
	 * Instantiates a new technical exception.
	 *
	 * @param message the message
	 */
	public InsureException(final String message) {
		super(message);
		this.message = message;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return message;
	}
}
