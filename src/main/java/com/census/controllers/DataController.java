package com.census.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.census.dto.AddEmailTemplateDto;
import com.census.dto.ContactUsDto;
import com.census.dto.EmailTemplateResponse;
import com.census.dto.ResponseDto;
import com.census.dto.UpdateEmailTemplateDto;
import com.census.dto.UserDtoTransform;
import com.census.model.County;
import com.census.model.EmailTemplate;
import com.census.model.Owners;
import com.census.model.Sic;
import com.census.repository.CountyRepository;
import com.census.repository.EmailTemplateRepository;
import com.census.repository.SicRepository;
import com.census.service.EmailService;
import com.census.utils.Constants;
import com.census.utils.EmailUtil;
import com.census.utils.MessageConstant;

import io.swagger.annotations.Api;

@CrossOrigin(origins = "*")
@RestController
@Api(tags = "Other insure data operations like sic..", value = "Data import Operations")
public class DataController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataController.class);

	@Autowired
	private SicRepository sicRepository;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private CountyRepository countryRepository;

	@Autowired
	private EmailTemplateRepository emailTemplateRepository;

	@Value("${contactt.us}")
	private String contactus;

	/**
	 * 
	 * @param sic
	 * @return
	 */
	@PostMapping("/addSic")
	public ResponseEntity<ResponseDto> addSic(@RequestBody Sic sic) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::addSic");
		try {
			sic = sicRepository.save(sic);
			if (sic != null) {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.SIC_RECORD_ADDED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while inserting SIC record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:addSic - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * 
	 * @return
	 */
	@GetMapping("/siclist")
	public ResponseEntity<ResponseDto> getSics() {
		ResponseDto rdto = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::getSics");
		try {
			List<Sic> findAll = sicRepository.findAll();
			if (findAll != null && findAll.size() > 0) {
				rdto.setStatus(Constants.SUCCESS);
				rdto.setData(findAll);
			}
			else
			{
				rdto.setStatus(Constants.SUCCESS);
				rdto.setMessage(MessageConstant.NO_DATA_FOUND);
				rdto.setData(findAll);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while getting SIC record.");
			rdto.setStatus(Constants.FAIL);
			return new ResponseEntity<>(rdto, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by DataController:getSics - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(rdto, HttpStatus.OK);
	}
	
	@PostMapping("/contactus")
	public ResponseEntity<ResponseDto> contactus(@RequestBody ContactUsDto contactUsDto) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::contactus");
		try {
			String emailBody = "";
			EmailTemplate email = emailTemplateRepository.findByTemplateId("constact_us");
			if(email != null)
			{
			if (contactUsDto.getName() != null) {
				emailBody = email.getMailBody() +" Name: " + contactUsDto.getName()
						+ "\n";
			}
			if (contactUsDto.getEmail() != null) {
				emailBody = emailBody + email.getMailBody()+" Email: "
						+ contactUsDto.getEmail() + "\n";
			}
			if (contactUsDto.getPhone() != null) {
				emailBody = emailBody + email.getMailBody()+" Phone: "
						+ contactUsDto.getPhone() + "\n";
			}
			if (contactUsDto.getMessage() != null) {
				emailBody = emailBody + "Message: " + contactUsDto.getMessage()
						+ "\n";
			}
			}
			LOGGER.info("contactus email started for user :"+contactUsDto.getEmail());

			EmailUtil.sendMail(contactus, email.getSubject(), emailBody);

			LOGGER.info("Update contactus email sent completed for user :" + contactUsDto.getEmail());

		} catch (Exception e) {
			LOGGER.error("Exception occured while contactus mail."+e.getStackTrace());
			response.setStatus(Constants.FAIL);
			response.setMessage("Mail not delivered, Mail server not found.");
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by contactus - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/county")
	public ResponseEntity<ResponseDto> getCountyListByZipcode(@RequestParam("zipcode") Integer zipcode) {
		ResponseDto rdto = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::getCountyListByZipcode");
		try {
			List<County> data = countryRepository.findByZipcode(zipcode);
			if (data != null) {
				rdto.setStatus(Constants.SUCCESS);
				rdto.setData(data);
			}
			else
			{
				rdto.setStatus(Constants.SUCCESS);
				rdto.setMessage(MessageConstant.NO_DATA_FOUND);
				rdto.setData(data);
			}
			
		} catch(Exception e)
		{
			LOGGER.error("Exception occured while getting county record.");
			rdto.setStatus(Constants.FAIL);
			return new ResponseEntity<>(rdto, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by  DataController:getCountyListByZipcode- {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(rdto, HttpStatus.OK);
	}
	
	@GetMapping("/getAllCounty")
	public ResponseEntity<ResponseDto> getCountyList() {
		ResponseDto rdto = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::getCountyListByZipcode");
		try {
			List<County> data = countryRepository.findAll();
			if (data != null) {
				rdto.setStatus(Constants.SUCCESS);
				rdto.setData(data);
			}
			else
			{
				rdto.setStatus(Constants.SUCCESS);
				rdto.setMessage(MessageConstant.NO_DATA_FOUND);
				rdto.setData(data);
			}
			
		} catch(Exception e)
		{
			LOGGER.error("Exception occured while getting county record.");
			rdto.setStatus(Constants.FAIL);
			return new ResponseEntity<>(rdto, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by  DataController:getCountyListByZipcode- {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(rdto, HttpStatus.OK);
	}
	
	@PostMapping("/addCounty")
	public ResponseEntity<ResponseDto> addCountry(@RequestBody County country) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::addSic");
		try {
			country = countryRepository.save(country);
			if (country != null) {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.SIC_RECORD_ADDED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while inserting country record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:addCountry - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/getZipcodeByStateName")
	public ResponseEntity<ResponseDto> getZipcodeByState(@RequestParam("state") String state) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::addSic");
		try {
			List<County> county = countryRepository.findByStateName(state);
			if(county != null)
			{
				response.setData(county);
				response.setStatus(Constants.SUCCESS);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while inserting country record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:addCountry - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/getEmailTemplate")
	public ResponseEntity<ResponseDto> getEmailTemplate() {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::getEmailTemplate");
		try {
			List<EmailTemplate> emailtemaplte = emailTemplateRepository.findAll();
			if (emailtemaplte != null) {
				response.setData(emailtemaplte);
				response.setStatus(Constants.SUCCESS);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while get EmailTemplate record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:getEmailTemplate - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/updateEmailTemplate")
	public ResponseEntity<ResponseDto> UpdateEmailTemplate(@RequestBody UpdateEmailTemplateDto updateEmailTemplateDto) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::UpdateEmailTemplate");
		try {
			EmailTemplate email = emailTemplateRepository.getOne(updateEmailTemplateDto.getId());
			if (email != null) {
				email.setSubject(updateEmailTemplateDto.getSubject());
				email.setMailBody(updateEmailTemplateDto.getMailBody());
				emailTemplateRepository.save(email);
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.UPDATED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while UpdateEmailTemplate record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:UpdateEmailTemplate - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/addEmailTemplate")
	public ResponseEntity<ResponseDto> addEmailTemplate(@RequestBody AddEmailTemplateDto addEmailTemplateDto) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		EmailTemplate email = new EmailTemplate();
		LOGGER.info("Calling DataController::UpdateEmailTemplate");
		try {
			email.setSubject(addEmailTemplateDto.getSubject());
			email.setMailBody(addEmailTemplateDto.getMailBody());
			email.setStatus(Constants.STATUS_ACTIVE);
			email.setTemplateId(addEmailTemplateDto.getTemplateId());
			email.setTemplateFor(addEmailTemplateDto.getTemplateFor());

			emailTemplateRepository.save(email);
			response.setStatus(Constants.SUCCESS);
			response.setMessage(MessageConstant.RECORD_ADDED_SUCCESSFULLY);

		} catch (Exception e) {
			LOGGER.error("Exception occured while UpdateEmailTemplate record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:UpdateEmailTemplate - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@GetMapping("/getEmailTemplateById")
	public ResponseEntity<ResponseDto> getEmailTemplateInfo(@RequestParam Long id) {
		ResponseDto response = new ResponseDto();
		EmailTemplateResponse dto = new EmailTemplateResponse();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling DataController::getEmailTemplate");
		try {
			EmailTemplate emailtemaplte = emailTemplateRepository.getOne(id);
			if (emailtemaplte != null) {
				dto.setId(emailtemaplte.getId());
				dto.setMailBody(emailtemaplte.getMailBody());
				dto.setSubject(emailtemaplte.getSubject());
				dto.setTemplateId(emailtemaplte.getTemplateId());
				dto.setTemplateFor(emailtemaplte.getTemplateFor());
				dto.setStatus(emailtemaplte.getStatus());
				dto.setCreatedAt(emailtemaplte.getCreatedAt());
				dto.setUpdatedAt(emailtemaplte.getUpdatedAt());
				dto.setUuid(emailtemaplte.getUuid());
				response.setData(dto);
				response.setStatus(Constants.SUCCESS);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while get EmailTemplate record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by DataController:getEmailTemplate - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}
