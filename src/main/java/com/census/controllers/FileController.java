package com.census.controllers;

import java.io.ByteArrayInputStream;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.census.dto.ResponseDto;
import com.census.model.User;
import com.census.service.CensusService;
import com.census.service.UserService;
import com.census.utils.CSVHelper;
import com.census.utils.Constants;
import com.census.utils.MessageConstant;

@CrossOrigin(origins = "*")
@RestController
public class FileController {

    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    
    @Autowired
    CensusService censusService;
    
    @Autowired
    UserService userService;


    @PostMapping("/upload")
    public ResponseEntity<ResponseDto> uploadFile(@RequestParam("importfile") MultipartFile importfile ,@RequestParam("groupId") String groupId) {
      String message = "";
      if (CSVHelper.hasCSVFormat(importfile)) {
        try {
        	User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        	censusService.save(importfile,user.getUsername(),groupId);

          message = MessageConstant.UPLOAD_FILE + importfile.getOriginalFilename();
          return ResponseEntity.status(HttpStatus.OK).body(new ResponseDto(Constants.SUCCESS,null,message,null));
        } catch (Exception e) {
          message = MessageConstant.FILE_NOT_UPLOADED + importfile.getOriginalFilename() + "!";
          logger.error(message+e.getMessage());
          return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseDto(Constants.FAIL,null,message,null));
        }
      }
      message = MessageConstant.UPLOAD_CSV_FILE;
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDto(Constants.FAIL,null,message,null));
    }
    
    @GetMapping("/download")
    public ResponseEntity<Resource> getFile(@RequestParam("groupId") String groupId) throws ParseException {
      String filename = groupId+"_census.csv";
      InputStreamResource file = new InputStreamResource(censusService.load(groupId));

      return ResponseEntity.ok()
          .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename)
          .contentType(MediaType.parseMediaType("application/csv"))
          .body(file);
    }

    //TODO
    @PostMapping("/attach")
    public ResponseEntity<ResponseDto> attachFile(@RequestParam("attachfile") MultipartFile attachfile ,@RequestParam("group_id") String group_id) {
    	  return ResponseEntity.status(HttpStatus.OK).body(new ResponseDto(Constants.SUCCESS,null,"The file Successfully attached.",null));
    }
}