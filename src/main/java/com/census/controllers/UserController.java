package com.census.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.census.admin.dto.UpdateSubAdminDtoRequest;
import com.census.admin.dto.UserListResponseDto;
import com.census.dto.AssistanceDtoRequest;
import com.census.dto.CreateUserResponse;
import com.census.dto.ResponseDto;
import com.census.dto.UpdateAssistantDtoRequest;
import com.census.dto.UpdateUserRequestDto;
import com.census.dto.UserDto;
import com.census.dto.UserDtoTransform;
import com.census.dto.UserProfileRequestDto;
import com.census.exception.InsureException;
import com.census.model.EmailTemplate;
import com.census.model.Role;
import com.census.model.User;
import com.census.model.UserProfile;
import com.census.repository.EmailTemplateRepository;
import com.census.repository.RoleRespository;
import com.census.repository.UserRepository;
import com.census.service.EmailService;
import com.census.service.UserProfileService;
import com.census.service.UserService;
import com.census.utils.Constants;
import com.census.utils.DateUtils;
import com.census.utils.EmailUtil;
import com.census.utils.MessageConstant;

import io.swagger.annotations.Api;

/**
 *
 * @author mahesh.jammula
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/user")
@Api(tags = "User operations for Census data access", value = "UserOperations")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRespository roleRespository;

	@Autowired
	private UserProfileService userProfileService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailTemplateRepository emailTemplateRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	private static final String BROKER = "Broker";

	@Value("${app.baseurl}")
	private String baseurl;

	/**
	 * Create user user.
	 *
	 *
	 * @return the user
	 */
	@PostMapping("/users")
	public ResponseEntity<CreateUserResponse> createUser(@Valid @RequestBody UserDto userDto) {
		CreateUserResponse response = new CreateUserResponse();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::createUser");
		try {
			Role role = roleRespository.findByRoleName("Manager");
			User userFromDTO = UserDtoTransform.getUserFromDTO(userDto);
			userFromDTO.setRole(role);
			User user = userService.save(userFromDTO, null);
			if (user != null && user.getAccType().equalsIgnoreCase(BROKER)) {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.USER_REGISTERED_SUCCESSFULLY);
				response.setUserId(user.getId());
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.USER_REGISTERED_SUCCESSFULLY);
			}
			activatemail(Constants.STATUS_ACTIVE, user.getUsername());
		} catch (Exception e) {
			LOGGER.error("Exception occured while creating user");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:createUser - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Get all users list.
	 *
	 * @return the list
	 */
	@GetMapping("/users")
	public ResponseEntity<ResponseDto> getAllUsers() {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling getAllUsers");
		try {
			List<User> findAll = userService.findUsers();
			if (findAll != null) {
				response.setStatus(Constants.SUCCESS);
				response.setData(findAll);
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(findAll);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while getting userprofile");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:getAllUsers - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@PostMapping("/role")
	public Role createRole(@RequestBody Role role) {
		return roleRespository.save(role);
	}

	/**
	 * Api to create registration page for broker.
	 *
	 * @return response
	 */
	@PostMapping("/usersProfile")
	public ResponseEntity<ResponseDto> createUserProfile(@RequestBody UserProfileRequestDto userProfileRequest) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::createUserProfile");
		try {
			User user = userService.findByUsername(null == userProfileRequest.getUserName()
					? SecurityContextHolder.getContext().getAuthentication().getName()
					: userProfileRequest.getUserName());
			if (null == user)
				throw new InsureException(MessageConstant.USER_DOES_NOT_EXIST);
			UserProfile userProfile = userProfileService.addUserProfile(userProfileRequest, user);
			if (userProfile != null) {
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.USER_REGISTERED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while creating userprofile");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:createUserProfile - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	/**
	 * Get all users.
	 *
	 * @return the User
	 */
	@GetMapping("/users/getProfile")
	public ResponseEntity<ResponseDto> getUserProfile(
			@RequestParam(value = "userName", required = false) String userName) {
		UpdateUserRequestDto userprofileres = new UpdateUserRequestDto();
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController:: getUserProfile");
		try {
			User user = userService.findByUsername(
					null == userName ? SecurityContextHolder.getContext().getAuthentication().getName() : userName);
//			UserProfile userProfile = userProfileRepository.findByUserId(user.getId());
//			if(user.getAccType().equalsIgnoreCase(BROKER) && user != null && userProfile != null)
//			{
			if (null != user) {
				UserProfile userProfile = user.getUserProfile();
				response.setStatus(Constants.SUCCESS);

				userprofileres.setPhone(user.getMobile());
				userprofileres.setEmail(user.getEmail());
				userprofileres.setAccType(user.getAccType());
				userprofileres.setUserId(user.getId());
				userprofileres.setUserName(user.getUsername());
				userprofileres.setStatus(user.getStatus());
				if (null != user.getUserProfile()) {
					userprofileres.setFirstName(userProfile.getFname());
					userprofileres.setLastName(userProfile.getLname());
					userprofileres.setLicenseNumber(userProfile.getLicenseNo());
					userprofileres.setLicenseType(userProfile.getLicenseType());
					userprofileres.setLicenseExpiry(null != userProfile.getLicenseExpiryDt()? DateUtils.convertDateIntoStringFormate(userProfile.getLicenseExpiryDt()): "-");
					userprofileres.setCompanyStruct(userProfile.getCoStructure());
					userprofileres.setAgencyName(userProfile.getAgencyName());
					userprofileres.setAddress(userProfile.getAddress());
					userprofileres.setState(userProfile.getState());
					userprofileres.setCity(userProfile.getCity());
					userprofileres.setZipcode(userProfile.getZipcode());
				}
				response.setData(userprofileres);
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setData(MessageConstant.NO_DATA_FOUND);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while getting UserProfile");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:getUserProfile - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Api to update the UserProfile.
	 *
	 * @return the response
	 */
	@PostMapping("/users/updateProfile")
	public ResponseEntity<ResponseDto> updateUserProfile(@RequestBody UpdateUserRequestDto updateUserRequestDto, Boolean isAdmin) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::updateUserProfile");
		try {
			User user = userService.findByUsername(StringUtils.isBlank(updateUserRequestDto.getUserName())
					? SecurityContextHolder.getContext().getAuthentication().getName()
					: updateUserRequestDto.getUserName());
			userService.updateUserProfile(user, updateUserRequestDto,isAdmin);
			status.setStatus(Constants.SUCCESS);
			status.setMessage(MessageConstant.USERPROFILE_UPDATED_SUCCESSFULLY);

		} catch (Exception e) {
			LOGGER.error("Exception occured while updating User Profile");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:updateUserProfile - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	/**
	 * Api to send email verification.
	 *
	 *
	 * @return the response
	 */

	@RequestMapping(value = "/activatemail", method = RequestMethod.POST)
	public ResponseDto activatemail(@RequestParam("status") String status,
			@RequestParam("userEmail") String userEmail) {
		User user = userService.findByUsername(userEmail);
		LOGGER.info("The activate Account processing started with baseurl:" + baseurl);
		try {
			if (user == null) {
				return new ResponseDto(Constants.FAIL, null, MessageConstant.EMAIL_DOES_NOT_EXIST, null);
			} else {
				if(user.getAccType().equalsIgnoreCase(Constants.BROKER))
				{
					EmailTemplate email = emailTemplateRepository.findByTemplateId("Broker_Register");
					if(email != null)
					{
					 EmailUtil.sendMail(user.getEmail(), email.getSubject(), email.getMailBody()+ baseurl + "/user" + "/activate?token=" + user.getUsername());
					}
				}
				else
				{
					EmailTemplate email = emailTemplateRepository.findByTemplateId("Employer_Register");
					if(email != null)
					{
						EmailUtil.sendMail(user.getEmail(), email.getSubject(), email.getMailBody()+ baseurl + "/user" + "/activate?token=" + user.getUsername());
					}
				}
			}
			return new ResponseDto(Constants.SUCCESS, null, MessageConstant.EMAIL_SENT, null);
		} catch (Exception e) {
			return new ResponseDto(Constants.SUCCESS, null, "Account not activated.Please contact system admin.", null);
		}
	}

	/**
	 * Api to activate account.
	 *
	 *
	 * @return the response
	 */

	@RequestMapping(value = "/activate", method = RequestMethod.GET)
	public String activate(@RequestParam("token") String token) {
		LOGGER.info("Calling LoginController::activate");
		User user = userRepository.findByUsername(token);
		if (user == null) {
			return MessageConstant.INVALID_RESET_LINK;
		}
		user.setStatus(Constants.STATUS_ACTIVE);
		userRepository.save(user);
		return "Your Account is activated successfully! Please login through application.";
	}

	/**
	 * Api to create Assistance.
	 *
	 *
	 * @return the response
	 */

	@PostMapping("/saveAssistance")
	public ResponseEntity<ResponseDto> saveAssistance(@RequestBody AssistanceDtoRequest assistance) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::saveAssistance");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			User savedto = userService.saveAssistant(assistance, user);
			if (savedto != null) {
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.RECORD_ADDED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while save assistance");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:saveAssistance - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	/**
	 * Api to get All Assistance.
	 *
	 *
	 * @return the response
	 */

	@GetMapping("/getAssistance")
	public ResponseEntity<ResponseDto> getAssistance() {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::getAssistance");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			List<UserListResponseDto> findAll = userService.getAssistant(user);
			if (findAll != null) {
				response.setStatus(Constants.SUCCESS);
				response.setData(findAll);
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(findAll);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while get Assistance");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:saveAssistance - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Api to get Assistance by Id.
	 *
	 *
	 * @return the response
	 */

	@GetMapping("/getAssistanceById")
	public ResponseEntity<ResponseDto> getAssistanceById(@RequestParam("id") Long userId) {
		ResponseDto response = new ResponseDto();
		UserListResponseDto dto = new UserListResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::getAssistanceById");
		try {
			User user = userRepository.getOne(userId);
			if (user != null) {
				response.setStatus(Constants.SUCCESS);
				dto.setEmail(user.getEmail());
				dto.setAccType(user.getAccType());
				dto.setMobile(user.getMobile());
				dto.setStatus(user.getStatus());
				dto.setCreatedAt(user.getCreatedAt());
				dto.setUpdatedAt(user.getUpdatedAt());
				response.setData(dto);
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(dto);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while getAssistanceById");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:getAssistanceById - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Api to update Assistance.
	 *
	 *
	 * @return the response
	 */

	@PostMapping("/updateAssistant")
	public ResponseEntity<ResponseDto> updateAssistant(@RequestBody UpdateAssistantDtoRequest updateAssistance) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::updateAssistant");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			User update = userService.updateAssistant(updateAssistance,user);
			if (update != null) {
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.UPDATED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while update Assistant");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:saveAssistance - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	/**
	 * Api to delete Assistance.
	 *
	 *
	 * @return the response
	 */

	@PostMapping("/deleteAssistant")
	public ResponseEntity<ResponseDto> deleteAssistant(@RequestParam("id") Long id) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::deleteAssistant");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			User deleteAssis = userRepository.findByParentUserAndUser(id, user.getId());
			if (deleteAssis != null) {
				userRepository.deleteById(deleteAssis.getId());
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.DELETE_RECORD);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while delete Assistant");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:delete Assistant - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}

	/**
	 * Api to update Assistance Password.
	 *
	 *
	 * @return the response
	 */

	@PostMapping("/updateAssistantPassword")
	public ResponseEntity<ResponseDto> updateAssistantPassword(@RequestBody UpdateSubAdminDtoRequest updatePassword) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::updateAssistantPassword");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			User Assis = userRepository.findByParentUserAndUser(updatePassword.getId(), user.getId());
			if (Assis != null) {
				userService.changeUserPassword(Assis, updatePassword.getPassword());
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.PASSWORD_CHANGED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while update Assistant Password");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:update Assistant Password - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(status, HttpStatus.OK);
	}
}
