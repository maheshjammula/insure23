package com.census.controllers;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.census.config.JwtTokenUtil;
import com.census.dto.ApiResponse;
import com.census.dto.LoginDto;
import com.census.dto.PassUpdateDto;
import com.census.dto.ResetPasswordDto;
import com.census.dto.ResponseDto;
import com.census.exception.InsureException;
import com.census.model.EmailTemplate;
import com.census.model.User;
import com.census.repository.EmailTemplateRepository;
import com.census.repository.UserRepository;
import com.census.service.EmailService;
import com.census.service.UserService;
import com.census.utils.CommonUtil;
import com.census.utils.Constants;
import com.census.utils.EmailUtil;
import com.census.utils.MessageConstant;

import io.swagger.annotations.Api;

/**
 *
 * @author mahesh.jammula
 *
 */
@CrossOrigin(origins = "*")
@RestController
@Api(tags = "User operations for Census data access", value = "UserOperations")
public class LoginController {

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private EmailService emailService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailTemplateRepository emailTemplateRepository;

	@Autowired
	private CommonUtil commonUtil;
	
	@Value("${app.baseurl}")
	private String baseurl;
	
	@Autowired
	private UserController userController;

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public Test welcomePage() {
		Test t = new Test();
		t.setContent("asdf");
		t.setId("123");
		return t;
	}

	/**
	 * Api to Update password.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/user/updatePassword")
	public ResponseDto changeUserPassword(@RequestBody PassUpdateDto loginDto) throws Exception {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling LoginController::changeUserPassword");
		User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if (!commonUtil.checkIfValidOldPassword(user.getEncPwd(), loginDto.getOldPassword())) {
			response.setStatus(Constants.FAIL);
			response.setMessage(MessageConstant.INVALID_PASSWORD);
			return response;
		}
		userService.changeUserPassword(user, loginDto.getPassword());
		response.setStatus(Constants.SUCCESS);
		response.setMessage(MessageConstant.PASSWORD_CHANGED_SUCCESSFULLY);
		return response;
	}


	/**
	 * Api to Login the user.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/login")
	public Object login(@RequestBody LoginDto loginDto) {
		LOGGER.info("Calling LoginController::login");
		try {
			ApiResponse user = userService.login(loginDto);
			user.setToken(jwtTokenUtil.generateToken(loginDto.getEmail()));
			return user;
		} 
		catch (InsureException ie) {
			userController.activatemail(null, loginDto.getEmail());
			return new ResponseDto(Constants.FAIL, null, ie.getMessage(), null);
		}
		catch (Exception e) {
			return new ResponseDto(Constants.FAIL, null, MessageConstant.USERNAME_PASSWORD_WRONG, null);
		}
	}

	/**
	 * Api to signout.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/signout")
	public String logoutDo(HttpServletRequest request,HttpServletResponse response){
		LOGGER.info("Calling LoginController::logoutDo");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			SecurityContextHolder.clearContext();
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}

		return MessageConstant.LOGOUT_SUCCESS ;
	}

	/**
	 * Api to forgot password.
	 *
	 *
	 * @return the response
	 * @throws Exception 
	 */

	@RequestMapping(value = "/forgot", method = RequestMethod.POST)
	public ResponseDto processForgotPasswordForm(@RequestParam("email") String userEmail, HttpServletRequest request) throws Exception {
		User user= userService.findByUsername(userEmail);
		LOGGER.info("The forgor password processing started with baseurl:"+baseurl);

		if(user == null)
		{
			return new ResponseDto(Constants.FAIL, null, MessageConstant.EMAIL_DOES_NOT_EXIST, null);
		} else {
			UUID uuid = UUID.randomUUID();
			user.setPwdResetCode(uuid.toString());
			userRepository.save(user);
			EmailTemplate email = emailTemplateRepository.findByTemplateId("forget_password");
			if(email != null)
			{
				EmailUtil.sendMail(user.getEmail(), email.getSubject(), email.getMailBody() + baseurl
						+ "/reset-password?token=" + user.getPwdResetCode());
			}
		}
		return new ResponseDto(Constants.SUCCESS, null, MessageConstant.EMAIL_SENT, null);

	}

	/**
	 * Api to reset password.
	 *
	 *
	 * @return the response
	 * @throws Exception 
	 */
	
	@RequestMapping(value = "/reset", method = RequestMethod.POST)
	public ResponseDto resetCustomerPassword(@RequestBody ResetPasswordDto resetPassword) throws Exception {
		LOGGER.info("Calling LoginController::resetCustomerPassword");
		User user = userRepository.findByPwdResetCode(resetPassword.getToken());
		if (user == null )
		{
			return new ResponseDto(Constants.FAIL, null, MessageConstant.INVALID_RESET_LINK, null);
		}
	    userService.changeUserPassword(user, resetPassword.getNewpassword()); 
	    LOGGER.info("Update password completed for user :"+user.getEmail());
	    user.setPassword(resetPassword.getNewpassword());
	    EmailTemplate email = emailTemplateRepository.findByTemplateId("reset_password");
		if(email != null)
		{
			 EmailUtil.sendMail(user.getEmail(), email.getSubject(), email.getMailBody());
		}
	    //updatePasswordMail(user);
		return new ResponseDto(Constants.SUCCESS, null, MessageConstant.RESET_PASSWORD_SUCCESSFULLY, null);
	    
	}

	/**
	 *  Sending mail to update password message
	 *
	 *
	 * @return void
	 */
	protected void updatePasswordMail(User user) {
		String content = MessageConstant.CONTENT_FIRST + user.getPassword();
		content += MessageConstant.CONTENT_SECOND + MessageConstant.CONTENT_THIRD;
		try {
			LOGGER.info("Update password email started for user :"+user.getEmail());
			EmailUtil.sendMail(user.getEmail(), MessageConstant.SUBJECT, content);
			LOGGER.info("Update password email sent completed for user :"+user.getEmail());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
