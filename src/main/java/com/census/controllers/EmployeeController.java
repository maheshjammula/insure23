package com.census.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.census.dto.CensusDeleteDto;
import com.census.dto.CensusResponseDto;
import com.census.dto.DependentInsertRequestDto;
import com.census.dto.EmployeeInsertRequestDto;
import com.census.dto.EmployeeResponseDto;
import com.census.dto.ResponseDto;
import com.census.dto.UpdateEmployeeRequestDto;
import com.census.model.CensusDependent;
import com.census.model.User;
import com.census.repository.CensusRepository;
import com.census.repository.DependentCensusRepository;
import com.census.service.EmployeeService;
import com.census.service.UserService;
import com.census.utils.Constants;
import com.census.utils.MessageConstant;

import io.swagger.annotations.Api;

@CrossOrigin(origins = "*")
@RestController
@Api(tags = "Employee operations for Census data access", value = "EmployeeOperations")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    UserService userService;

    @Autowired
    DependentCensusRepository dependentCensusRepository;

    @Autowired
    CensusRepository censusRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    /**
	 * Api to add the employee details
	 *
	 *
	 * @return the employee response
	 */
	@PostMapping("/saveEmployee")
	public ResponseEntity<ResponseDto> addEmployee(@RequestParam("group_id") Long groupId,@RequestBody List<EmployeeInsertRequestDto> employeeInsertList) {
		ResponseDto dto = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling EmployeeController::addEmployee");
		try {
			if (employeeInsertList != null && employeeInsertList.size() > 0) {
				for (EmployeeInsertRequestDto list : employeeInsertList) {
					User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
					if (list.getId() == null) {
						employeeService.addEmployee(list, user,groupId);
					} else {
						employeeService.updateEmployeeWithDependents(list);
					}

				}
				dto.setStatus(Constants.SUCCESS);
				dto.setMessage(MessageConstant.RECORD_ADDED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while inserting employee record.");
			dto.setStatus(Constants.FAIL);
			dto.setMessage(e.getMessage());
			return new ResponseEntity<>(dto, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by EmployeeController:addEmployee or updatingEmployees - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

    /**
     * Api to add the employee dependents details
     *
     *
     * @return the employee response
     */

    @PostMapping("/addDependent")
    public ResponseEntity<ResponseDto> addDependent(@RequestBody DependentInsertRequestDto dependentInsertRequestDto)
    {
    	ResponseDto response = new ResponseDto();
        EmployeeResponseDto dto = new EmployeeResponseDto();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Calling EmployeeController::addDependent");
        try
        {
            CensusDependent censusDependent = employeeService.addDependent(dependentInsertRequestDto);
            if(censusDependent != null)
            {
                response.setStatus(Constants.SUCCESS);
                response.setMessage(MessageConstant.RECORD_ADDED_SUCCESSFULLY);
                dto.setId(censusDependent.getId());
                response.setData(dto);
            }

        } catch (Exception e)
        {
            LOGGER.error("Exception ouccrred while inserting employee dependent record.");
            response.setStatus(Constants.FAIL);
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        LOGGER.debug("Time taken by EmployeeController:addDependent - {}",
                System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Api to get the employee list.
     *
     *
     * @return the employee response
     */
    @GetMapping("/getEmployees")
    public ResponseEntity<ResponseDto> getEmployee(@RequestParam(value = "groupId", required=false) String groupId)
	{
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling EmployeeController::getEmployee");
		try {
			List<CensusResponseDto> censusList = employeeService.getEmployee(groupId);
			if (censusList != null) {
				response.setStatus(Constants.SUCCESS);
				response.setData(censusList);
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(censusList);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while getting employee records.");
			response.setStatus(Constants.FAIL);
			return new ResponseEntity<>(response, HttpStatus.OK);

		}
		LOGGER.debug("Time taken by EmployeeController:getEmployee - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

    /**
     * Api to get the employee list.
     *
     *
     * @return the employee response
     */
    @GetMapping("/getEmployeeInfoById")
    public ResponseEntity<ResponseDto> getEmployeeInfoById(@RequestParam(value = "id") Long id)
    {
        ResponseDto response = new ResponseDto();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Calling EmployeeController::getEmployee");
        try{
            CensusResponseDto censusList = employeeService.getEmployeeInfoById(id);
            if(censusList != null)
            {
                response.setStatus(Constants.SUCCESS);
                response.setData(censusList);
            }

        } catch (Exception e)
        {
            LOGGER.error("Exception occured while getting employee records.");
            response.setStatus(Constants.FAIL);
            return new ResponseEntity<>(response, HttpStatus.OK);

        }
        LOGGER.debug("Time taken by EmployeeController:getEmployee - {}",
                System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    /**
     * Api to delete the employee record
     *
     *
     * @return the employee response
     */

    @PostMapping("/deleteEmployeeById")
    public ResponseEntity<ResponseDto> deleteEmployee(@RequestBody CensusDeleteDto censusDeleteDto)
    {
    	ResponseDto response = new ResponseDto();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Calling EmployeeController::deleteEmployee");
        try{
            boolean isremoved = employeeService.deleteEmployee(censusDeleteDto);
            if(isremoved)
            {
                response.setStatus(Constants.SUCCESS);
                response.setMessage(MessageConstant. DELETE_RECORD);
            }

        } catch (Exception e)
        {
            LOGGER.error("Exception occured while deleting employee records.");
            response.setStatus(Constants.FAIL);
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.OK);

        }
        LOGGER.debug("Time taken by EmployeeController:deleteEmployee - {}",
                System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Api to delete the dependent record
     *
     *
     * @return the employee response
     */
    @PostMapping("/deleteDependentById")
    public ResponseEntity<ResponseDto> deleteDependent(@RequestBody CensusDeleteDto censusDeleteDto)
    {
        ResponseDto response  = new ResponseDto();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Calling EmployeeController::deleteDependent");
        try{
            CensusDependent censusDependent = dependentCensusRepository.getOne(censusDeleteDto.getId());
            if( censusDependent != null)
            {
                dependentCensusRepository.deleteById(censusDependent.getId());
                response.setStatus(Constants.SUCCESS);
                response.setMessage(MessageConstant. DELETE_RECORD);
            }

        } catch(Exception e)
        {
            LOGGER.error("Exception occured while deleting employee dependent records.");
            response.setStatus(Constants.FAIL);
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response, HttpStatus.OK);
        }
        LOGGER.debug("Time taken by EmployeeController:deleteEmployee - {}",
                System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Api to update the employee record
     *
     *
     * @return the response
     */

    @PostMapping("/updateEmployee")
    public ResponseEntity<ResponseDto> updateEmployee(@RequestBody UpdateEmployeeRequestDto updateEmployeeRequestDto)
    {
    	ResponseDto response  = new ResponseDto();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Calling EmployeeController::updateEmployee");
        try{
            employeeService.updateEmployee(updateEmployeeRequestDto);
            response.setStatus(Constants.SUCCESS);
            response.setMessage(MessageConstant.UPDATED_SUCCESSFULLY);


        } catch(Exception e)
        {
            LOGGER.error("Exception occured while updating employee records.");
        	response.setStatus(Constants.FAIL);
            response.setMessage(e.getMessage());
            return new ResponseEntity<>(response,HttpStatus.OK);

        }
        LOGGER.debug("Time taken by EmployeeController:updateEmployee - {}",
                System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    /**
     * Api to update the dependent record
     *
     *
     * @return the response
     */
    @PostMapping("/updateDependent")
    public ResponseEntity<ResponseDto> updateDependent(@RequestBody DependentInsertRequestDto updateDependentRequestDto)
    {
    	ResponseDto response  = new ResponseDto();
        long startTime = System.currentTimeMillis();
        LOGGER.info("Calling EmployeeController::updateDependent");
          try{
              employeeService.updateCensusDependent(updateDependentRequestDto);
              response.setStatus(Constants.SUCCESS);
              response.setMessage(MessageConstant.UPDATED_SUCCESSFULLY);

          } catch(Exception e)
          {
              LOGGER.error("Exception occured while updating employee dependent.");
        	  response.setStatus(Constants.FAIL);
              response.setMessage(e.getMessage());
              return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
          }
        LOGGER.debug("Time taken by EmployeeController:updateDependent - {}",
                System.currentTimeMillis() - startTime);
        return new ResponseEntity<>(response,HttpStatus.OK);
    }



}
