package com.census.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.census.dto.OwnersRequestDto;
import com.census.dto.ResponseDto;
import com.census.dto.ShareGroupRequestDto;
import com.census.dto.UserDtoTransform;
import com.census.exception.InsureException;
import com.census.model.History;
import com.census.model.Owners;
import com.census.model.Sic;
import com.census.model.User;
import com.census.model.UserGroup;
import com.census.repository.HistoryRepository;
import com.census.repository.OwnersRepository;
import com.census.repository.SicRepository;
import com.census.repository.UserGroupRepository;
import com.census.repository.UserRepository;
import com.census.service.OwnersService;
import com.census.service.UserService;
import com.census.utils.Constants;
import com.census.utils.MessageConstant;

import io.swagger.annotations.Api;

@CrossOrigin(origins = "*")
@RestController
@Api(tags = "Owners operations for group data access", value = "OwnersOperations")
public class OwnersController {

	@Autowired
	private OwnersService ownersService;
	
	@Autowired
	private SicRepository sicRepository;

	@Autowired
	private OwnersRepository ownersRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private HistoryRepository historyRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private UserGroupRepository userGroupRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(OwnersController.class);

	/**
	 * Api to create group.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/addGroup")
	public ResponseEntity<ResponseDto> addGroup(@RequestBody OwnersRequestDto ownersRequestDto) {
		UserGroup userGroup = new UserGroup();
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::addGroup");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			Owners owners = ownersService.addOwners(user, ownersRequestDto);
			if (owners != null) {
				userGroup.setOwners(owners);
				if(user.getParentUser() != null)
				{
					userGroup.setManagerId(user.getParentUser().getId());
				}
				else
				{
					userGroup.setManagerId(user.getId());
				}
				userGroup.setUser(user);
				userGroupRepository.save(userGroup);
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.GROUP_ADDED_SUCCESSFULLY);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while creating group");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by OwnersController:addGroup - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
	}

	/**
	 * Api to Update group.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/editGroup")
	public ResponseEntity<ResponseDto> editGroup(@RequestBody OwnersRequestDto ownersRequestDto) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::editGroup");
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			if (null == user)
				throw new InsureException(MessageConstant.USER_DOES_NOT_EXIST);
			Owners owners = ownersService.updateOwners(user, ownersRequestDto);
			if (owners != null) {
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.GROUP_UPDATED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while Updating group");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by OwnersController:editGroup - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
	}

	/**
	 * Api to get groups.
	 *
	 *
	 * @return the group response.
	 */
	@GetMapping("/groups")
	public ResponseEntity<ResponseDto> getGroups() {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::getGroups");
		try {
			List<Owners> findAll = ownersService.findAll();
			if (findAll != null && findAll.size() > 0) {
				response.setStatus(Constants.SUCCESS);
				response.setData(findAll);
			}
			else
			{
				response.setStatus(Constants.SUCCESS);
				response.setStatus(MessageConstant.NO_DATA_FOUND);
				response.setData(findAll);
				
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while getting group");
			response.setStatus(Constants.FAIL);
			return new ResponseEntity<>(new ResponseDto(Constants.FAIL, null, e.getMessage(), null), HttpStatus.OK);
		}
		LOGGER.debug("Time taken by OwnersController:getGroups - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
	}

	/**
	 * Api to get user group.
	 *
	 *
	 * @return the group response
	 */
	@GetMapping("/getUserGroups")
	public ResponseEntity<ResponseDto> getUserGroups() {
		ResponseDto rdto = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::getUserGroups");
		Long managerId = null;
		try {
			User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
			if(user.getParentUser() != null)
			{
				managerId = user.getParentUser().getId();
			}
			else
			{
				managerId = user.getId();
			}
			
			List<UserGroup> list = userGroupRepository.findByManager(managerId).get();
			if (list != null && list.size() > 0) {
				List<Owners> owners = list.stream().map(p -> p.getOwners()).filter(p -> p.getStatus() != null && !p.getStatus().equalsIgnoreCase("Deleted")).collect(Collectors.toList());
				rdto.setStatus(Constants.SUCCESS);
				rdto.setData(owners);
			}
			

			  
		} catch (Exception e) {
			LOGGER.error("Exception occured while getting User Groups");
			rdto.setStatus(Constants.FAIL);
			rdto.setMessage(e.getMessage());
			rdto.setData(null);
			return new ResponseEntity<>(rdto, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by OwnersController:getUserGroups - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(rdto, HttpStatus.OK);

	}

	/**
	 * Api to get the group by groupId.
	 *
	 *
	 * @return the response
	 */
	@GetMapping("/groupsInfoById")
	public ResponseEntity<ResponseDto> getGroupsInfo(@RequestParam Long id) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::getGroupsInfoById");
		try {
			Owners data = ownersRepository.getOne(id);
			if (data != null) {
				response.setData(UserDtoTransform.getGroupInforFromModel(data));
				response.setStatus(Constants.SUCCESS);
				return new ResponseEntity<>(response, HttpStatus.OK);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while getting group information by id");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by OwnersController:getUserGroups - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Api to share the groups with others users.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/shareGroup")
	public ResponseEntity<ResponseDto> ShareGroupToOtherUsers(@RequestBody ShareGroupRequestDto shareGroupRequestDto) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::ShareGroupToOtherUsers");
		try {
			UserGroup userGroup = ownersService.shareGroupToUsers(shareGroupRequestDto);
			if (userGroup != null)
				response.setStatus(Constants.SUCCESS);

		} catch (Exception e) {
			LOGGER.error("Exception occured while share group to other users");
			response.setStatus(Constants.FAIL);
			response.setMessage(MessageConstant.GROUP_AND_USER_NOT_EXIST);
			response.setData(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by OwnersController:getUserGroups - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/**
	 * Api to share the groups with others users.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/delete/shareGroup")
	public ResponseEntity<ResponseDto> shareGroupDelete(@RequestParam("historyId") Long historyId) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::shareGroupDelete");
		try {
			History history = historyRepository.getOne(historyId);
			if(history != null)
			{
				User user = userRepository.findByEmail(history.getSharedTo());
				UserGroup userGroup = userGroupRepository.findByUserIdAndGroupId(user.getId(),
						history.getGroupId());
				if(userGroup != null ) {
					userGroupRepository.delete(userGroup);
					history.setStatus("InActive");
					historyRepository.save(history);
					response.setStatus(Constants.SUCCESS);
					
				}
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting share group.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by shareGroupHistory - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	/**
	 * Api to get share the groups details.
	 *
	 *
	 * @return the response
	 */
	@GetMapping("/shareGroupHistory")
	public ResponseEntity<ResponseDto> shareGroupHistory(@RequestParam("groupId") Long groupId) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling shareGroupHistory::shareGroupHistory");
		try {
			List<History> list = historyRepository.findByGroupId(groupId);
			List<History> historyList = list.stream().filter(a-> a.getStatus().equalsIgnoreCase("Active")).collect(Collectors.toList());
			if( historyList != null && historyList.size() > 0)
			{
				response.setStatus(Constants.SUCCESS);
				response.setData(historyList);
			}
			else
			{
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting share group.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by shareGroupDelete - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}
	
	@PostMapping("/updateGroupSic")
	public ResponseEntity<ResponseDto> updateGroup(@RequestBody OwnersRequestDto ownersRequestDto) {
		ResponseDto status = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling OwnersController::editGroup");
		try {
			Owners	owners = ownersRepository.getOne(ownersRequestDto.getId());
			Sic sic = sicRepository.findById(ownersRequestDto.getSic()).get();
			owners.setSic(sic);
			ownersRepository.save(owners);
			if (owners != null) {
				status.setStatus(Constants.SUCCESS);
				status.setMessage(MessageConstant.GROUP_UPDATED_SUCCESSFULLY);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while Updating group");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by updateGroup - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
	}

}
