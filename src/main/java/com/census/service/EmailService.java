package com.census.service;

import org.springframework.mail.SimpleMailMessage;

/**
 * 
 * @author mahesh.jammula
 *
 */
public interface EmailService {
	
	void sendEmail(SimpleMailMessage email);
	

}
