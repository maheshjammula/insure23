package com.census.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.census.admin.dto.UserListResponseDto;
import com.census.admin.model.AdminUser;
import com.census.admin.repository.AdminUserRepository;
import com.census.dto.ApiResponse;
import com.census.dto.AssistanceDtoRequest;
import com.census.dto.LoginDto;
import com.census.dto.UpdateAssistantDtoRequest;
import com.census.dto.UpdateUserRequestDto;
import com.census.exception.InsureException;
import com.census.model.Role;
import com.census.model.User;
import com.census.model.UserProfile;
import com.census.repository.RoleRespository;
import com.census.repository.UserProfileRepository;
import com.census.repository.UserRepository;
import com.census.utils.CommonUtil;
import com.census.utils.Constants;
import com.census.utils.DateUtils;
import com.census.utils.MessageConstant;
import com.census.utils.Verbosity;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Service
public class UserServiceImpl implements UserService {
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private CommonUtil commonUtil;

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Autowired
	private AdminUserRepository adminUserRepository;

	@Autowired
	private RoleRespository roleRespository;

	@Override
	public User save(User user, String role) throws Exception {
		User isUserExist = findByUsername(user.getEmail());
		if (isUserExist == null) {
			user.setEncPwd(new BCryptPasswordEncoder().encode(user.getPassword()));
			user.setAccType(StringUtils.isEmpty(user.getAccType()) ? Constants.USER : user.getAccType());
			user.setStatus(Constants.STATUS_INACTIVE);
//        Role userRole = roleRespository.findByRoleName(role);
//        user.setRole(userRole);
			userRepository.save(user);
			LOGGER.info("User with id: {} created", user.getId());

		} else {
			LOGGER.error("User already exist.");
			throw new InsureException("User already exist.");
		}
		return user;
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByEmail(username);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public ApiResponse login(LoginDto loginDto) throws InsureException {
		User user = findByUsername(loginDto.getEmail());
		if (user == null) {
			throw new RuntimeException("User does not exist.");
		}
		if (user.getStatus().equalsIgnoreCase(Constants.STATUS_INACTIVE)) {
			throw new InsureException("User account is not active!");
		}
		if (user.getStatus().equalsIgnoreCase(Constants.STATUS_DELETED)) {
			throw new InsureException("User account is deleted.");
		}
		if (new BCryptPasswordEncoder().matches(loginDto.getPassword(), user.getEncPwd())) {

			UserProfile findByUserId = userProfileRepository.findByUserId(user.getId());
			User findByParentId = userRepository.findByParentUserAndRole(user.getId());
			Boolean brokerExist = false;
			if (null != findByUserId) {
				brokerExist = true;
				if (findByParentId != null) {
					User isActive = userRepository.findByUserAndStatus(findByParentId.getParentUser().getId());
					if(isActive.getStatus().equalsIgnoreCase(Constants.STATUS_ACTIVE))
					{
					return new ApiResponse(Constants.SUCCESS, MessageConstant.LOGIN_SUCCESS, null, "Assistant",
							brokerExist);
					}
					else
					{
						throw new InsureException("User account is not active!");
					}
				} else {
					return new ApiResponse(Constants.SUCCESS, MessageConstant.LOGIN_SUCCESS, null, user.getAccType(),
							brokerExist);
				}
			} else {
				if (findByParentId != null) {
					User isActive = userRepository.findByUserAndStatus(findByParentId.getParentUser().getId());
					if(isActive != null)
					{
					  return new ApiResponse(Constants.SUCCESS, MessageConstant.LOGIN_SUCCESS, null, "Assistant",
							brokerExist);
					}
					else
					{
						throw new InsureException("User account is not active!");
					}
				} else {
					return new ApiResponse(Constants.SUCCESS, MessageConstant.LOGIN_SUCCESS, null, user.getAccType(),
							brokerExist);
				}
			}

		}

		return null;

	}

	@Override
	public void changeUserPassword(final User user, final String password) {
		user.setEncPwd(commonUtil.getEncodedPassword(password));
		userRepository.save(user);
		if (user.getAccType().equalsIgnoreCase(Constants.ADMIN)) {
			AdminUser adminUser = adminUserRepository.findByUsername(user.getUsername());
			adminUser.setEncPwd(commonUtil.getEncodedPassword(password));
			adminUserRepository.save(adminUser);
		}

		LOGGER.info("Password updated successfully");
	}

	@Override
	public void updateUserProfile(final User user, final UpdateUserRequestDto updateUserRequestDto , Boolean isAdmin)
			throws ParseException {
		UserProfile userProfile = userProfileRepository.findByUserId(user.getId());
		if (userProfile != null && user.getAccType().equalsIgnoreCase("Broker")) {
			userProfile.setFname(updateUserRequestDto.getFirstName());
			userProfile.setLname(updateUserRequestDto.getLastName());
			userProfile.setCoStructure(updateUserRequestDto.getCompanyStruct());
			userProfile.setLicenseNo(updateUserRequestDto.getLicenseNumber());
			userProfile.setLicenseType(updateUserRequestDto.getLicenseType());
			userProfile.setAgencyName(updateUserRequestDto.getAgencyName());
			userProfile.setLicenseExpiryDt(DateUtils.convertStringIntoDateFormate(updateUserRequestDto.getLicenseExpiry()));
			userProfile.setAddress(updateUserRequestDto.getAddress());
			userProfile.setCity(updateUserRequestDto.getCity());
			userProfile.setState(updateUserRequestDto.getState());
			userProfile.setZipcode(updateUserRequestDto.getZipcode());
			userProfileRepository.save(userProfile);
			LOGGER.info("User profile with id {} updated", userProfile.getId());
		}
		user.setMobile(updateUserRequestDto.getPhone());
		if(null != isAdmin && isAdmin) {
		user.setStatus(updateUserRequestDto.getStatus());
		}
		userRepository.save(user);
		LOGGER.info("User with id {} updated", user.getId());

	}

	@Override
	public List<User> findUsers() {
		List<User> findAllByOrderByIdDesc = userRepository.findAllByOrderByIdDesc();
		List<User> usersList = findAllByOrderByIdDesc.stream()
				.filter(a -> !a.getAccType().equalsIgnoreCase(Constants.ADMIN)).collect(Collectors.toList());
		return usersList;
	}

	public User saveAssistant(AssistanceDtoRequest assistance, User user) {
		User saveassistance = new User();
		UUID uuid = UUID.randomUUID();
		User isExist = userRepository.findByEmail(assistance.getEmail());
		if (isExist == null) {
			Role role = roleRespository.findByRoleName("Member");
			saveassistance.setEmail(assistance.getEmail());
			saveassistance.setUsername(assistance.getEmail());
			saveassistance.setAccType(Verbosity.findByAbbr(assistance.getAccType()).getText());
			saveassistance.setMobile(assistance.getPhone());
			saveassistance.setPassword(assistance.getPassword());
			saveassistance.setParentUser(user);
			saveassistance.setRole(role);
			saveassistance.setEncPwd(new BCryptPasswordEncoder().encode(assistance.getPassword()));
			saveassistance.setStatus(assistance.getStatus());
			saveassistance.setCreatedAt(new Date());
			saveassistance.setUpdatedAt(new Date());
			saveassistance.setUuid(uuid.toString());
			userRepository.save(saveassistance);
		}

		return saveassistance;
	}

	public List<UserListResponseDto> getAssistant(User user) {
		List<UserListResponseDto> userResponse = new ArrayList<>();
		List<User> findAllByOrderByIdDesc = userRepository.getAllAssistanceByParentId(user.getId());
		for (User dto : findAllByOrderByIdDesc) {
			UserListResponseDto response = new UserListResponseDto();
			response.setId(dto.getId());
			response.setUuid(dto.getUuid());
			response.setUsername(dto.getUsername());
			response.setRole(dto.getRole().getRoleName());
			response.setEmail(dto.getEmail());
			response.setMobile(dto.getMobile());
			response.setAccType(dto.getAccType());
			response.setStatus(dto.getStatus());
			response.setCreatedAt(dto.getCreatedAt());
			response.setUpdatedAt(dto.getUpdatedAt());
			userResponse.add(response);
		}

		return userResponse;

	}

	public User updateAssistant(UpdateAssistantDtoRequest updateAssistance, User user) {
		User assistant = userRepository.findByParentUserAndUser(updateAssistance.getId(), user.getId());
		if (assistant != null) {
			assistant.setEmail(updateAssistance.getEmail());
			assistant.setUsername(updateAssistance.getEmail());
			assistant.setMobile(updateAssistance.getPhone());
			assistant.setStatus(updateAssistance.getStatus());
			userRepository.save(assistant);
		}
		return assistant;

	}
}