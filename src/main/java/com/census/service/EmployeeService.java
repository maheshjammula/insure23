package com.census.service;

import java.util.List;

import com.census.dto.CensusDeleteDto;
import com.census.dto.CensusResponseDto;
import com.census.dto.DependentInsertRequestDto;
import com.census.dto.EmployeeInsertRequestDto;
import com.census.dto.UpdateEmployeeRequestDto;
import com.census.model.Census;
import com.census.model.CensusDependent;
import com.census.model.User;

public interface EmployeeService {

    public Census addEmployee(EmployeeInsertRequestDto employeeInsertRequestDto, User user,Long groupId) throws Exception;

    public CensusDependent addDependent(DependentInsertRequestDto dependentInsertRequestDto) throws Exception;

    public List<CensusResponseDto> getEmployee(String groupId) throws Exception;

    public Census updateEmployee(UpdateEmployeeRequestDto updateEmployeeRequestDto)throws Exception;

    public CensusDependent updateCensusDependent(DependentInsertRequestDto updateDependentRequestDto) throws  Exception;

    public boolean deleteEmployee(CensusDeleteDto censusDeleteDto) throws Exception;

    public CensusResponseDto getEmployeeInfoById(Long id) throws Exception;
    
    public Census updateEmployeeWithDependents(EmployeeInsertRequestDto updateEmployeeRequestDto) throws Exception; 
}
