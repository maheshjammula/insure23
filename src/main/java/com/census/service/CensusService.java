package com.census.service;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.census.model.Census;
import com.census.model.CensusDependent;
import com.census.repository.CensusRepository;
import com.census.repository.DependentCensusRepository;
import com.census.utils.CSVHelper;

@Service
public class CensusService {
  @Autowired
  CensusRepository repository;
  
  
  @Autowired
  DependentCensusRepository drepository;

  public void save(MultipartFile file, String userName, String groupId) {
    try {
      List<Census> censuses = CSVHelper.csvToCensus(file.getInputStream(),userName, groupId);
      censuses = repository.saveAll(censuses);
      for(Census census:censuses) {
    	  Set<CensusDependent> dependentCensus = census.getDependentCensus();
    	  for(CensusDependent cd :dependentCensus) {
    		  cd.setCensus(census);;
    		  drepository.save(cd);
    	  }
      }
    } catch (Exception e) {
      throw new RuntimeException("fail to store csv data: " + e.getMessage());
    }
  }

  public ByteArrayInputStream load(String groupId) throws ParseException {
	List<Census> censuses = null;
	if(null == groupId) {
    censuses = repository.findAll();
	}
	else {
	censuses = repository.findByGroupId(Long.valueOf(groupId));
	}

    ByteArrayInputStream in = CSVHelper.censusToCSV(censuses);
    return in;
  }
}
