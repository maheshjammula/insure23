package com.census.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.census.dto.UserProfileRequestDto;
import com.census.model.User;
import com.census.model.UserProfile;
import com.census.repository.UserProfileRepository;
import com.census.utils.DateUtils;


@Service
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    private UserProfileRepository userProfileRepo;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileServiceImpl.class);

    @Override
    public UserProfile addUserProfile(UserProfileRequestDto userRequest, User user) throws Exception {
        UserProfile userProfile = new UserProfile();
        UserProfile userexist = userProfileRepo.findByUserId(user.getId());
        if(null != userexist) throw new Exception("User already Exists");
            userProfile.setUser(user);
            userProfile.setFname(userRequest.getFirstName());
            userProfile.setLname(userRequest.getLastName());
            userProfile.setLicenseType(userRequest.getLicenseType());
            userProfile.setLicenseNo(userRequest.getLicenseNumber());
            userProfile.setLicenseExpiryDt(DateUtils.convertStringIntoDateFormate(userRequest.getLicenseExpiry()));
            userProfile.setCoStructure(userRequest.getCompanyStruct());
            userProfile.setAgencyName(userRequest.getAgencyName());
            userProfile.setAddress(userRequest.getAddress());
            userProfile.setZipcode(userRequest.getZipcode());
            userProfile.setState(userRequest.getState());
            userProfile.setCity(userRequest.getCity());
            userProfile.setStatus("Active");

            userProfileRepo.save(userProfile);

            LOGGER.info("User created with id: {} is created", userProfile.getId());

        return userProfile;
    }
}
