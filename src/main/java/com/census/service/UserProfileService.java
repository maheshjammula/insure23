package com.census.service;

import java.text.ParseException;

import com.census.dto.UserProfileRequestDto;
import com.census.model.User;
import com.census.model.UserProfile;

public interface UserProfileService {

    public UserProfile addUserProfile(UserProfileRequestDto userProfileRequest, User user) throws ParseException, Exception;
}
