package com.census.service;

import java.text.ParseException;
import java.util.List;

import com.census.admin.dto.UserListResponseDto;
import com.census.dto.ApiResponse;
import com.census.dto.AssistanceDtoRequest;
import com.census.dto.LoginDto;
import com.census.dto.UpdateAssistantDtoRequest;
import com.census.dto.UpdateUserRequestDto;
import com.census.exception.InsureException;
import com.census.model.User;


public interface UserService {
	
	User save(User user , String role) throws Exception;

    User findByUsername(String username);
    
    List<User> findAll();

	ApiResponse login(LoginDto loginDto) throws InsureException;

	void changeUserPassword(User user, String password);

	List<User> findUsers();

	void updateUserProfile(User user, UpdateUserRequestDto updateUserRequestDto, Boolean isAdmin) throws ParseException;

	User saveAssistant(AssistanceDtoRequest assistance, User user);

	List<UserListResponseDto> getAssistant(User user);

	User updateAssistant(UpdateAssistantDtoRequest updateAssistance, User user);
}

