package com.census.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.census.dto.CensusDeleteDto;
import com.census.dto.CensusDependentResponseDto;
import com.census.dto.CensusResponseDto;
import com.census.dto.DependentInsertRequestDto;
import com.census.dto.EmployeeInsertRequestDto;
import com.census.dto.UpdateEmployeeRequestDto;
import com.census.model.Census;
import com.census.model.CensusDependent;
import com.census.model.Owners;
import com.census.model.User;
import com.census.repository.CensusRepository;
import com.census.repository.DependentCensusRepository;
import com.census.repository.OwnersRepository;
import com.census.utils.DateUtils;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	OwnersRepository ownersRepository;

	@Autowired
	CensusRepository censusRepository;

	@Autowired
	DependentCensusRepository dependentCensusRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

	@Override
	public Census addEmployee(EmployeeInsertRequestDto employeeInsertRequestDto, User user, Long groupId) throws Exception {
		Census census = new Census();
		Owners owners = ownersRepository.getOne(groupId);
		if (owners != null) {
			census.setFname(employeeInsertRequestDto.getFirstName());
			census.setLname(employeeInsertRequestDto.getLastName());
			census.setGender(employeeInsertRequestDto.getGender());
			census.setAge(employeeInsertRequestDto.getAge());
//			census.setDob(DateUtils.convertStringIntoDateFormate(employeeInsertRequestDto.getDob()));
			getAgeCal(census, employeeInsertRequestDto.getDob());
			census.setZipcode(employeeInsertRequestDto.getZipcode());
			census.setCounty(employeeInsertRequestDto.getCountry());
			census.setCobraFlag(employeeInsertRequestDto.getCobra());
			census.setSpouseFlag(employeeInsertRequestDto.getSpouse());
			census.setChildFlag(employeeInsertRequestDto.getChild());
			census.setGroupId(groupId);
			census.setUsername(user.getUsername());
			census.setQuote(employeeInsertRequestDto.getQuote());
			census.setStatus("Active");

			census = censusRepository.save(census);

			if (null != employeeInsertRequestDto.getDependents() && employeeInsertRequestDto.getDependents().size() > 0) {
				List<DependentInsertRequestDto> dependents = employeeInsertRequestDto.getDependents();
				for (DependentInsertRequestDto depDto : dependents) {
					depDto.setId(census.getId());
					addDependent(depDto);
				}
			}

			LOGGER.info("Employee with id: {} is created", census.getId());
		} else {
			LOGGER.error("Group id is not exist.");
			throw new Exception("Unable to process request. Please try again later!");
		}
		return census;
	}
	
	private void getAgeCal(Census census, String dob) throws ParseException {
		if(!StringUtils.isEmpty(dob) && !StringUtils.isEmpty(dob.trim())){
			census.setDob(DateUtils.convertStringIntoDateFormate(dob));
			LocalDate birthDate = LocalDate.parse(dob);
			int calculateAge = DateUtils.calculateAge(birthDate);
			census.setAge(String.valueOf(calculateAge));
		}
	}
	
	//dependent age cal
	private void getAgeCal(CensusDependent census, String age) throws ParseException {
		if(!StringUtils.isEmpty(age) && !StringUtils.isEmpty(age.trim())){
			census.setDob(DateUtils.convertStringIntoDateFormate(age));
			LocalDate birthDate = LocalDate.parse(age);
			int calculateAge = DateUtils.calculateAge(birthDate);
			census.setAge(String.valueOf(calculateAge));
		}
	}

	@Override
	public CensusDependent addDependent(DependentInsertRequestDto dependentInsertRequestDto) throws Exception {
		CensusDependent censusDependent = new CensusDependent();
		try {
			Census census = censusRepository.getOne(dependentInsertRequestDto.getId());
			if (census != null) {
				censusDependent.setFname(dependentInsertRequestDto.getFirstName());
				censusDependent.setLname(dependentInsertRequestDto.getLastName());
				censusDependent.setGender(dependentInsertRequestDto.getGender());
				censusDependent.setAge(dependentInsertRequestDto.getAge());
				getAgeCal(censusDependent, dependentInsertRequestDto.getDob());
//				censusDependent.setDob(DateUtils.convertStringIntoDateFormate(dependentInsertRequestDto.getDob()));
				censusDependent.setRelation(dependentInsertRequestDto.getType());
				censusDependent.setCensus(census);
				censusDependent.setStatus("Active");

				dependentCensusRepository.save(censusDependent);

				/*
				 * if(dependentInsertRequestDto.getType().equalsIgnoreCase("spouse") &&
				 * census.getSpouseFlag().equalsIgnoreCase("false")) {
				 * census.setSpouseFlag("true"); censusRepository.save(census); }
				 * if(dependentInsertRequestDto.getType().equalsIgnoreCase("child") &&
				 * census.getChildFlag().equalsIgnoreCase("false")) {
				 * census.setChildFlag("true"); censusRepository.save(census); }
				 */
				LOGGER.info("Employee dependent with id: {} is created", censusDependent.getId());
			}
		} catch (Exception e) {
			LOGGER.error("Employee dependent with id: {} is not created.Error is {}", censusDependent.getId(),e.getMessage());
			throw new Exception("Unable to process request. Please try again later!");
		}

		return censusDependent;
	}

	@Override
	public List<CensusResponseDto> getEmployee(String groupId) throws Exception {

		List<CensusResponseDto> censusResponseList = new ArrayList<>();

		List<Census> censusList = (null == groupId) ? censusRepository.findAll()
				: censusRepository.findByGroupId(Long.valueOf(groupId));
		for (Census dataList : censusList) {
			CensusResponseDto censusResponseDto = new CensusResponseDto();
			// CensusDependentResponseDto censusDependentResponseDto = null;
			List<CensusDependentResponseDto> censusDependentResponseList = new ArrayList<>();

			for (CensusDependent censusDependent : dataList.getDependentCensus()) {
				CensusDependentResponseDto censusDependentResponseDto = new CensusDependentResponseDto();
				censusDependentResponseDto.setId(censusDependent.getId());
				censusDependentResponseDto.setFirstName(censusDependent.getFname());
				censusDependentResponseDto.setLastName(censusDependent.getLname());
				censusDependentResponseDto.setGender(censusDependent.getGender());
				if(null != censusDependent.getDob()) {
				censusDependentResponseDto.setDob(DateUtils.convertDateIntoStringFormate(censusDependent.getDob()));
				}
				censusDependentResponseDto.setAge(censusDependent.getAge());
				censusDependentResponseDto.setType(censusDependent.getRelation());

				censusDependentResponseList.add(censusDependentResponseDto);

			}
			censusResponseDto.setId(dataList.getId());
			censusResponseDto.setFirstName(dataList.getFname());
			censusResponseDto.setLastName(dataList.getLname());
			censusResponseDto.setGender(dataList.getGender());
			if(null != dataList.getDob()) {
			censusResponseDto.setDob(DateUtils.convertDateIntoStringFormate(dataList.getDob()));
			}
			censusResponseDto.setCountry(dataList.getCounty());
			censusResponseDto.setZipcode(dataList.getZipcode());
			censusResponseDto.setSpouse(dataList.getSpouseFlag());
			censusResponseDto.setChild(dataList.getChildFlag());
			censusResponseDto.setCobra(dataList.getCobraFlag());
			censusResponseDto.setAge(dataList.getAge());
			censusResponseDto.setQuote(dataList.getQuote());
			censusResponseDto.setDependents(censusDependentResponseList);

			censusResponseList.add(censusResponseDto);

		}

		LOGGER.info("Get the all employee and dependent list");
		return censusResponseList;
	}

	@Override
	public Census updateEmployee(UpdateEmployeeRequestDto updateEmployeeRequestDto) throws Exception {

		Census census = new Census();
		try {
			census = censusRepository.getOne(updateEmployeeRequestDto.getId());
			if (census != null) {
				if (updateEmployeeRequestDto.getSpouse().equalsIgnoreCase("false")) {
					List<CensusDependent> data = dependentCensusRepository.findByCensusAndRelation(census.getId(),"spouse");
					for( CensusDependent id : data )
					{
						dependentCensusRepository.deleteById(id.getId());
					}
				}

				if(updateEmployeeRequestDto.getChild().equalsIgnoreCase("false") )
				{
					List<CensusDependent> data = dependentCensusRepository.findByCensusAndRelation(census.getId(),"dependent");
					for( CensusDependent id : data )
					{
						dependentCensusRepository.deleteById(id.getId());
					}
				}
				census.setFname(updateEmployeeRequestDto.getFirstName());
				census.setLname(updateEmployeeRequestDto.getLastName());
				census.setGender(updateEmployeeRequestDto.getGender());
				census.setAge(updateEmployeeRequestDto.getAge());
				getAgeCal(census, updateEmployeeRequestDto.getDob());
//				census.setDob(DateUtils.convertStringIntoDateFormate(updateEmployeeRequestDto.getDob()));
				census.setCounty(updateEmployeeRequestDto.getCountry());
				census.setZipcode(updateEmployeeRequestDto.getZipcode());
				census.setSpouseFlag(updateEmployeeRequestDto.getSpouse());
				census.setCobraFlag(updateEmployeeRequestDto.getCobra());
				census.setChildFlag(updateEmployeeRequestDto.getChild());

				censusRepository.save(census);

				LOGGER.info("Employee with id: {} is updated", census.getId());

			}

		} catch (Exception e) {
			LOGGER.error("Employee id does Not exist.");
			throw e;
		}
		return census;
	}


	@Override
	public CensusDependent updateCensusDependent(DependentInsertRequestDto updateDependentRequestDto) throws Exception {

		CensusDependent censusDependent = new CensusDependent();
		try {

			censusDependent = dependentCensusRepository.getOne(updateDependentRequestDto.getId());
			if (censusDependent != null) {
				{
					censusDependent.setFname(updateDependentRequestDto.getFirstName());
					censusDependent.setLname(updateDependentRequestDto.getLastName());
					censusDependent.setGender(updateDependentRequestDto.getGender());
					censusDependent.setAge(updateDependentRequestDto.getAge());
					getAgeCal(censusDependent, updateDependentRequestDto.getDob());
//					censusDependent.setDob(DateUtils.convertStringIntoDateFormate(updateDependentRequestDto.getDob()));
					censusDependent.setRelation(updateDependentRequestDto.getType());

					dependentCensusRepository.save(censusDependent);

				}
			}
			LOGGER.info("Employee dependent with id: {} is updated", censusDependent.getId());
		} catch (Exception e) {
			LOGGER.error("Employee Dependent id does Not exist.");
			throw new Exception("Unable to process request. Please try again later!");
		}

		return censusDependent;
	}

	@Override
	public boolean deleteEmployee(CensusDeleteDto censusDeleteDto) throws Exception {
		Census census = censusRepository.getOne(censusDeleteDto.getId());
		if (census == null)
			throw new Exception("Id is not exist.");
		boolean isdelete = true;
		censusRepository.deleteById(census.getId());
		return isdelete;
	}

	@Override
	public CensusResponseDto getEmployeeInfoById(Long id) throws Exception {
		CensusResponseDto response = new CensusResponseDto();

		Census census = censusRepository.getOne(id);
		if(census != null) {
			List<CensusDependentResponseDto> censusDependentResponseList = new ArrayList<>();
			if(census.getDependentCensus() != null) {
				for (CensusDependent censusDependent : census.getDependentCensus()) {
					CensusDependentResponseDto censusDependentResponseDto = new CensusDependentResponseDto();
					censusDependentResponseDto.setId(censusDependent.getId());
					censusDependentResponseDto.setFirstName(censusDependent.getFname());
					censusDependentResponseDto.setLastName(censusDependent.getLname());
					censusDependentResponseDto.setGender(censusDependent.getGender());
					if(null != censusDependent.getDob()) {
					censusDependentResponseDto.setDob(DateUtils.convertDateIntoStringFormate(censusDependent.getDob()));
					}
					censusDependentResponseDto.setType(censusDependent.getRelation());
					censusDependentResponseDto.setAge(censusDependent.getAge());
					censusDependentResponseList.add(censusDependentResponseDto);

				}
			}
			response.setId(census.getId());
			response.setFirstName(census.getFname());
			response.setLastName(census.getLname());
			response.setGender(census.getGender());
			if(null != census.getDob()) {
			response.setDob(DateUtils.convertDateIntoStringFormate(census.getDob()));
			}
			response.setCountry(census.getCounty());
			response.setZipcode(census.getZipcode());
			response.setSpouse(census.getSpouseFlag());
			response.setChild(census.getChildFlag());
			response.setCobra(census.getCobraFlag());
			response.setAge(census.getAge());
			response.setQuote(census.getQuote());
			response.setDependents(censusDependentResponseList);

		}

		LOGGER.info("Get employee details and dependent.");
		return response;

	}
	
	@Override
	public Census updateEmployeeWithDependents(EmployeeInsertRequestDto updateEmployeeRequestDto) throws Exception {
		Census census = new Census();
		try {
			    census = censusRepository.getOne(updateEmployeeRequestDto.getId());
			    if(census != null)
			    {
				census.setFname(updateEmployeeRequestDto.getFirstName());
				census.setLname(updateEmployeeRequestDto.getLastName());
				census.setGender(updateEmployeeRequestDto.getGender());
				census.setAge(updateEmployeeRequestDto.getAge());
				getAgeCal(census, updateEmployeeRequestDto.getDob());
//				census.setDob(DateUtils.convertStringIntoDateFormate(updateEmployeeRequestDto.getDob()));
				census.setCounty(updateEmployeeRequestDto.getCountry());
				census.setZipcode(updateEmployeeRequestDto.getZipcode());
				census.setSpouseFlag(updateEmployeeRequestDto.getSpouse());
				census.setCobraFlag(updateEmployeeRequestDto.getCobra());
				census.setChildFlag(updateEmployeeRequestDto.getChild());
				census.setQuote(updateEmployeeRequestDto.getQuote());
				censusRepository.save(census);
				
			if (null != updateEmployeeRequestDto.getDependents()
					&& updateEmployeeRequestDto.getDependents().size() > 0) {
				List<DependentInsertRequestDto> dependents = updateEmployeeRequestDto.getDependents();
				for (DependentInsertRequestDto depDto : dependents) {
					//CensusDependent cenusDependents = dependentCensusRepository.getOne(depDto.getId());
					if (depDto.getId() == null) {
						depDto.setId(census.getId());
						addDependent(depDto);
					} else {
						updateCensusDependent(depDto);
					}
				}
			}
			    }
			LOGGER.info("Employee with id: {} is updated", census.getId());

		} catch (Exception e) {
			LOGGER.error("Employee id does Not exist.");
			throw new Exception("Unable to process request. Please try again later!");
		}
		return census;
	}

}
