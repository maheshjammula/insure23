package com.census.service;

import java.util.List;

import com.census.dto.OwnersRequestDto;
import com.census.dto.OwnersResponseDto;
import com.census.dto.ShareGroupRequestDto;
import com.census.model.Owners;
import com.census.model.User;
import com.census.model.UserGroup;

public interface OwnersService {

	public Owners addOwners(User user, OwnersRequestDto ownersRequestDto) throws Exception;

	public Owners updateOwners(User user, OwnersRequestDto ownersRequestDto) throws Exception;

	public List<Owners> findAll();

	public List<OwnersResponseDto> getGroupInformation();

	public UserGroup shareGroupToUsers(ShareGroupRequestDto shareGroupRequestDto) throws Exception;
}