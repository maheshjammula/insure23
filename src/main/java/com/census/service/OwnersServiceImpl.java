package com.census.service;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.census.dto.OwnersRequestDto;
import com.census.dto.OwnersResponseDto;
import com.census.dto.ShareGroupRequestDto;
import com.census.exception.InsureException;
import com.census.model.History;
import com.census.model.Owners;
import com.census.model.Sic;
import com.census.model.User;
import com.census.model.UserGroup;
import com.census.repository.HistoryRepository;
import com.census.repository.OwnersRepository;
import com.census.repository.SicRepository;
import com.census.repository.UserGroupRepository;
import com.census.repository.UserRepository;
import com.census.utils.Constants;
import com.census.utils.MessageConstant;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Service
public class OwnersServiceImpl implements OwnersService {

	@Autowired
	OwnersRepository ownersRepository;

	@Autowired
	UserGroupRepository userGroupRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private HistoryRepository historyRepository;

	@Autowired
	private SicRepository sicRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(OwnersServiceImpl.class);

	@Override
	public Owners addOwners(User user, OwnersRequestDto ownersRequestDto) throws Exception {
		Owners owners = new Owners();
		Owners ownersexist = ownersRepository.findByGroupName(ownersRequestDto.getName());
		Sic sic = sicRepository.findById(ownersRequestDto.getSic()).get();
		if(ownersexist == null ) {
				owners.setGroupName(ownersRequestDto.getName());
				owners.setSic(sic);
				owners.setCounty(ownersRequestDto.getCountry());
				owners.setZipcode(ownersRequestDto.getZip());
				owners.setEmpCount(ownersRequestDto.getEmployees());
				owners.setStatus(ownersRequestDto.getStatus());
				owners.setCreatedBy(user.getEmail());
				owners.setAccountType(ownersRequestDto.getAccountType());
				if(!user.getAccType().equalsIgnoreCase(Constants.ADMIN)) {
					owners.setAccountType(user.getAccType());
				}
				ownersRepository.save(owners);
			  LOGGER.info("Owner with id: {} is created", owners.getId());
			} else {
			LOGGER.error("Group name already exist.");
			throw new Exception("Group name already exist.");
		}
		return owners;
	}

	@Override
	public Owners updateOwners(User user, OwnersRequestDto ownersRequestDto) throws Exception {
		Owners	owners = ownersRepository.getOne(ownersRequestDto.getId());
			if(owners != null)
			{
				
				Owners ownersexist = ownersRepository.findByGroupName(ownersRequestDto.getName());
				Sic sic = sicRepository.findById(ownersRequestDto.getSic()).get();
				if(ownersexist != null && ownersexist.getId() != ownersRequestDto.getId()) throw new Exception("Group name already exist.");
				owners.setGroupName(ownersRequestDto.getName());
				owners.setSic(sic);
				owners.setEmpCount(ownersRequestDto.getEmployees());
				owners.setCounty(ownersRequestDto.getCountry());
				owners.setZipcode(ownersRequestDto.getZip());
				owners.setUpdatedBy(user.getEmail());
				owners.setAccountType(ownersRequestDto.getAccountType());
				owners.setStatus(ownersRequestDto.getStatus());
				if(!user.getAccType().equalsIgnoreCase(Constants.ADMIN)) {
					owners.setAccountType(user.getAccType());
				}
				ownersRepository.save(owners);
				LOGGER.info("Owner with id: {} is updated", owners.getId());
			} else
		    {
				LOGGER.error("Group is not exist.");
				throw new Exception("Group is not exist.");
		    }
		return owners;
	}

	@Override
	public List<Owners> findAll() {
		// TODO Auto-generated method stub
		LOGGER.info("Returned the list of owners");
		return ownersRepository.findAll();
	}

	@Override
	public List<OwnersResponseDto> getGroupInformation() {
		List<Owners> findAll = ownersRepository.findAll();
         List<OwnersResponseDto> ownersData = new ArrayList<OwnersResponseDto>();
		for (Owners data : findAll){
			OwnersResponseDto response = new OwnersResponseDto();
			response.setId(data.getId());
			response.setName(data.getGroupName());
			response.setCountry(data.getCounty());
			response.setEmployees(data.getEmpCount());
			response.setZip(data.getZipcode());
			response.setSic(data.getSic().getId());
			response.setSicName(data.getSic().getSicName());
			response.setSicCode(data.getSic().getSicCode());
			response.setAccType(data.getAccountType());
			response.setStatus(data.getStatus());
			ownersData.add(response);
			LOGGER.info("Returned the list of owners");
		}
		return ownersData;
	}

	@Override
	@Transactional
	public UserGroup shareGroupToUsers(ShareGroupRequestDto shareGroupRequestDto) throws Exception {

		UserGroup userGroup = null;
		List<Long> listid = Stream.of(shareGroupRequestDto.getGroupslist().split(","))
				.map(Long::parseLong)
				.collect(Collectors.toList());
		User user = userRepository.findByEmail(shareGroupRequestDto.getEmail());
		List<Owners> owners = ownersRepository.findByIdIn(listid);
		if(user != null && owners != null)
		{
			for(Owners data : owners)
			{
				userGroup = new UserGroup();
				userGroup.setUser(user);
				userGroup.setOwners(data);
				
				Boolean isShared = userGroupRepository.findByUserAndGroupId(user.getId(), data.getId()).isPresent()?true:false;
				if(!isShared) {
				userGroupRepository.save(userGroup);
				History entity = new History();
				entity.setGroupId(data.getId());
				entity.setSharedBy(SecurityContextHolder.getContext().getAuthentication().getName());
				entity.setSharedTo(user.getUsername());
				entity.setStatus("active");
				historyRepository.save(entity);
			}
			}
			LOGGER.info("Share the groups with other users.");
		}
		else {
			throw new InsureException(MessageConstant.SHARED_USER_DOES_NOT_EXIST);
		}

		return userGroup;
	}
}