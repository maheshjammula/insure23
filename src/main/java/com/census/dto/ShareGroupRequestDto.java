package com.census.dto;

import org.springframework.stereotype.Component;

@Component
public class ShareGroupRequestDto {

    private String email;
    private String groupslist;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGroupslist() {
        return groupslist;
    }

    public void setGroupslist(String groupslist) {
        this.groupslist = groupslist;
    }
}
