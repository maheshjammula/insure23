package com.census.dto;

import java.util.Date;

import com.census.admin.dto.AdminUserDto;
import com.census.admin.model.AdminUser;
import com.census.model.Owners;
import com.census.model.User;
import com.census.utils.Constants;
import com.census.utils.Verbosity;

/**
 * 
 * @author mahesh.jammula
 *
 */
public class UserDtoTransform {

	public static User getUserFromDTO(UserDto userDto) {

		User user = new User();
//		user.setPwdResetCode(userDto.getPwdResetCode());
		user.setAccType(Verbosity.findByAbbr(userDto.getAccount()).getText());
		user.setMobile(userDto.getPhone());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());
		user.setStatus(userDto.getAcceptance());
		user.setUsername(userDto.getEmail());
		user.setCreatedAt(new Date());
		user.setUpdatedAt(new Date());
//		user.setRole(userDto.getRole());
		return user;

	}

	public static OwnersResponseDto getGroupInforFromModel(Owners data) {
		OwnersResponseDto response = new OwnersResponseDto();
		response.setId(data.getId());
		response.setName(data.getGroupName());
		response.setSic(data.getSic().getId());
		response.setSicName(data.getSic().getSicName());
		response.setSicCode(data.getSic().getSicCode());
		response.setCountry(data.getCounty());
		response.setEmployees(data.getEmpCount());
		response.setZip(data.getZipcode());
		response.setAccType(data.getAccountType());
		response.setStatus(data.getStatus());

		return response;
	}

	public static AdminUser getAdminUserFromDTO(AdminUserDto userDto) {

		AdminUser user = new AdminUser();
//		user.setPwdResetCode(userDto.getPwdResetCode());
		user.setPassword(userDto.getPassword());
		user.setStatus(userDto.getStatus());
		user.setUsername(userDto.getEmail());
		user.setAccType(Constants.ADMIN);
		return user;

	}

}
