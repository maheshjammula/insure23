package com.census.dto;

import org.springframework.stereotype.Component;

@Component
public class DependentInsertRequestDto {

    private String firstName;
    private String lastName;
    private String gender;
    private String dob;
    private String type;
    private Long id;
    private String age;

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public String getDob() { return dob; }

    public void setDob(String dob) { this.dob = dob; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
    
    
}
