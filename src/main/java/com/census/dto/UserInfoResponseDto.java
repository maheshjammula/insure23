package com.census.dto;

import java.util.List;

public class UserInfoResponseDto {

    private String status;
    private List<UpdateUserRequestDto> userRequestDtos;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<UpdateUserRequestDto> getUserRequestDtos() {
        return userRequestDtos;
    }

    public void setUserRequestDtos(List<UpdateUserRequestDto> userRequestDtos) {
        this.userRequestDtos = userRequestDtos;
    }
}
