package com.census.dto;

import org.springframework.stereotype.Component;

@Component
public class OwnersRequestDto {

	private Long id;
	private String name;
	private Long zip;
	private String country;
	private Long sic;
	private Long employees;
	private String accountType;
	private String status;

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	public Long getZip() { return zip; }

	public void setZip(Long zip) { this.zip = zip; }

	public String getCountry() { return country; }

	public void setCountry(String country) { this.country = country; }

	public Long getSic() { return sic; }

	public void setSic(Long sic) { this.sic = sic; }

	public Long getEmployees() { return employees; }

	public void setEmployees(Long employees) { this.employees = employees; }

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
