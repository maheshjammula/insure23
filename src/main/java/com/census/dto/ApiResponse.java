package com.census.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiResponse {

    private String status;
    private String message;
    private String token;
    
    @JsonProperty("acc_type")
    private String accType;
    
    @JsonProperty("broker_registration")
    private boolean brokerRegistration= false;


    public ApiResponse(String status, String message, String token,String accType,boolean brokerRegistration){
        this.status = status;
        this.message = message;
        this.token = token;
        this.accType = accType;
        this.brokerRegistration = brokerRegistration;
    }

	public String getStatus() { return status; }

	public void setStatus(String status) { this.status = status; }

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

   

}