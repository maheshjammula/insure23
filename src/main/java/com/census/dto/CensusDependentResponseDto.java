package com.census.dto;

import org.springframework.stereotype.Component;

@Component
public class CensusDependentResponseDto {

    private Long id;
    private String type;
    private String firstName;
    private String lastName;
    private String gender;
    private String dob;
    private String age;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public String getDob() { return dob; }

    public void setDob(String dob) { this.dob = dob; }

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

   
}
