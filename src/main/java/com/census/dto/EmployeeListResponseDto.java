package com.census.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class EmployeeListResponseDto {

    private String status;
    private List<CensusResponseDto> data = new ArrayList<>();

    public String getStatus() { return status; }

    public void setStatus(String status) { this.status = status; }

    public List<CensusResponseDto> getData() { return data; }

    public void setData(List<CensusResponseDto> data) { this.data = data; }

    @Override
    public String toString() {
        return "EmployeeListResponseDto{" +
                "status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
