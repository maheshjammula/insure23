package com.census.dto;

import org.springframework.stereotype.Component;

@Component
public class CensusDeleteDto {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
