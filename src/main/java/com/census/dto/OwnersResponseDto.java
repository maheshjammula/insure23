package com.census.dto;

public class OwnersResponseDto{

    private Long id;
    private String name;
    private Long zip;
    private String country;
    private Long sic;
    private Long employees;
    private String sicName;
    private String sicCode;;
    private String AccType;
    private String status;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getZip() {
        return zip;
    }

    public void setZip(Long zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getSic() { return sic; }

    public void setSic(Long sic) { this.sic = sic; }

    public String getSicName() { return sicName; }

    public void setSicName(String sicName) { this.sicName = sicName; }

    public Long getEmployees() {
        return employees;
    }

    public void setEmployees(Long employees) {
        this.employees = employees;
    }

	public String getSicCode() {
		return sicCode;
	}

	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

    public String getAccType() { return AccType; }

    public void setAccType(String accType) { AccType = accType; }

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    

}
