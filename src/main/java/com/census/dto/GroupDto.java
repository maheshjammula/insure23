package com.census.dto;

public class GroupDto {

	private Long id;
	private String name;
	private String info;
	private String census;
	private String coapp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getCensus() {
		return census;
	}

	public void setCensus(String census) {
		this.census = census;
	}

	public String getCoapp() {
		return coapp;
	}

	public void setCoapp(String coapp) {
		this.coapp = coapp;
	}

}
