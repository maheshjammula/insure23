package com.census.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AssistanceDtoRequest {
	
	@NotNull
	private String email;
	
	@NotNull
	private String password;
	
	private Long phone;
	
	@NotNull
	@Size(min=2, message = "Account type should not empty")
	private String accType;
	
	private String status;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
