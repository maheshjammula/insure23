package com.census.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class EmployeeInsertRequestDto {

    private String firstName;
    private String lastName;
    private String gender;
    private String dob;
    private Integer zipcode;
    private String country;
    private String cobra;
    private String spouse;
    private String child;
   // private Long group_id;
    private String age;
    private Long id;
    private String quote;
    
    private List<DependentInsertRequestDto> dependents = new ArrayList<>();

    //for admin update
    private String userName;

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public String getDob() { return dob; }

    public void setDob(String dob) { this.dob = dob; }

    public Integer getZipcode() { return zipcode; }

    public void setZipcode(Integer zipcode) { this.zipcode = zipcode; }

    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }

    public String getCobra() { return cobra; }

    public void setCobra(String cobra) { this.cobra = cobra; }

    public String getSpouse() { return spouse; }

    public void setSpouse(String spouse) { this.spouse = spouse; }

    public String getChild() { return child; }

    public void setChild(String child) { this.child = child; }

	/*
	 * public Long getGroup_id() { return group_id; }
	 * 
	 * public void setGroup_id(Long group_id) { this.group_id = group_id; }
	 */

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }

	public List<DependentInsertRequestDto> getDependents() {
		return dependents;
	}

	public void setDependents(List<DependentInsertRequestDto> dependents) {
		this.dependents = dependents;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}
    
}
