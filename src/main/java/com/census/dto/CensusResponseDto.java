package com.census.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CensusResponseDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String dob;
    private Integer zipcode;
    private String country;
    private String cobra;
    private String spouse;
    private String child;
    private String age;
    private String quote;
    private List<CensusDependentResponseDto> dependents = new ArrayList<CensusDependentResponseDto>();

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }

    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public String getDob() { return dob; }

    public void setDob(String dob) { this.dob = dob; }

    public Integer getZipcode() { return zipcode; }

    public void setZipcode(Integer zipcode) { this.zipcode = zipcode; }

    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }

    public String getCobra() { return cobra; }

    public void setCobra(String cobra) { this.cobra = cobra; }

    public String getSpouse() { return spouse; }

    public void setSpouse(String spouse) { this.spouse = spouse; }

    public String getChild() { return child; }

    public void setChild(String child) { this.child = child; }

    public List<CensusDependentResponseDto> getDependents() { return dependents; }

    public void setDependents(List<CensusDependentResponseDto> dependents) { this.dependents = dependents; }

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}
}
