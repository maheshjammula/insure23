package com.census.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.census.model.EmailTemplate;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long> {
	
	EmailTemplate findByTemplateId(String templateId);

}
