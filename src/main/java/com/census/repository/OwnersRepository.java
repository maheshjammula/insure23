package com.census.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.census.model.Owners;

public interface OwnersRepository extends JpaRepository<Owners, Long> {
	Owners findByGroupName(String groupName);

	List<Owners> findByIdIn(List<Long> id);

	List<Owners> findAllByOrderByIdDesc();

	@Query("SELECT COUNT(*) FROM Owners o WHERE o.status !='Deleted' OR o.status !='inactive'")
	Long getOwnersCount();
	
	@Query("SELECT o.groupName FROM Owners o WHERE o.id =?1")
	String findByGroupId(Long id);
}