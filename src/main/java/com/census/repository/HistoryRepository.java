package com.census.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.census.model.History;

/**
 * 
 * @author mahesh.jammula
 *
 */
public interface HistoryRepository extends JpaRepository<History, Long> {

    List<History> findAllByOrderByIdDesc();
    List<History> findByGroupId(Long groupId);

}
