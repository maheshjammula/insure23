package com.census.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.census.model.User;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);
	
	User findByUsername(String username);

	User findByPwdResetCode(String pwdResetCode);

	List<User> findAllByOrderByIdDesc();

	@Query(value = "select * from users where parent_user_id = ?1 ", nativeQuery = true)
	List<User> findByParentUser(Long parentId);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.accType =?1")
	Long getUserTypeCount(String accType);
	
	public Optional<User> findById(Long userId);
	
	@Query(value = "SELECT u.* FROM users as u LEFT JOIN admin_users as au ON u.uname = au.uname WHERE au.acc_type = 'admin' order by u.id desc ", nativeQuery = true)
	List<User> getAllSubAdmins();
	
	@Query(value = "select * from users where parent_user_id = ?1 ", nativeQuery = true)
	List<User> getAllUserByParentId(Long userId);

	@Query(value = "select * from users where parent_user_id = ?1 AND acc_type != 'admin' order by id desc ", nativeQuery = true)
	List<User> getAllAssistanceByParentId(Long parentId);

	@Query(value = "select * from users where id = ?1 AND role_id = 2 ", nativeQuery = true)
	User findByParentUserAndRole(Long userId);

	@Query(value = "select * from users where id = ?1 AND parent_user_id = ?2 ", nativeQuery = true)
	User findByParentUserAndUser(Long userId, Long parentId);

	@Query(value = "select * from users where id = ?1 AND status = 'active' ", nativeQuery = true)
	User findByUserAndStatus(Long userId);
}
