package com.census.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.census.model.CensusDependent;

public interface DependentCensusRepository extends JpaRepository<CensusDependent, Long> {

	@Query(value = "select * from dependant_census where census_id = ?1", nativeQuery = true)
	List<CensusDependent> findByCensus(Long censusId);

	ArrayList<CensusDependent> findByIdIn(ArrayList<Long> id);

	@Query(value = "select * from dependant_census where census_id = ?1 And relation = ?2 ", nativeQuery = true)
	List<CensusDependent> findByCensusAndRelation(Long censusId, String relation);
	
	@Query(value ="select count(*) FROM dependant_census d JOIN census c ON d.census_id = c.id WHERE c.status ='Active'", nativeQuery = true)
	Long getCensusDependentCount();


}