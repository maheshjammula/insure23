package com.census.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.census.model.UserGroup;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Integer> {

	@Query("SELECT t FROM UserGroup t where t.user.id = ?1")
	public Optional<List<UserGroup>> findByUser(Long userId);
	
	@Query("SELECT t FROM UserGroup t where t.user.id = ?1 and t.owners.id = ?2")
	public Optional<List<UserGroup>> findByUserAndGroupId(Long userId, Long groupId);
	
	@Query("SELECT t FROM UserGroup t where t.owners.id = ?1")
	List<UserGroup> findByOwners(Long groupId);
	
	@Query(value = "select * from user_group where manager_id =?1 ", nativeQuery = true)
	public Optional<List<UserGroup>> findByManager(Long managerId);
	
	@Query("SELECT t FROM UserGroup t where t.user.id = ?1 and t.owners.id = ?2")
	public UserGroup findByUserIdAndGroupId(Long userId, Long groupId);

}
