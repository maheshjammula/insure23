package com.census.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.census.model.Role;

@Repository
public interface RoleRespository extends JpaRepository<Role, Integer> {

 Role findByRoleName(String role);
}