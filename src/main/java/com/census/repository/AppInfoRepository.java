package com.census.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.census.model.AppInfo;

public interface AppInfoRepository extends JpaRepository<AppInfo, Long>{

}
