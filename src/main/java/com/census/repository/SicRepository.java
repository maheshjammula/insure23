package com.census.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.census.model.Sic;
/**
 * 
 * @author mahesh.jammula
 *
 */
@Repository
public interface SicRepository extends JpaRepository<Sic, Long>{
	
	Sic findBySicName(String sickName);

}
