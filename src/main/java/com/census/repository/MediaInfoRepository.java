package com.census.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.census.model.MediaInfo;

/**
 * 
 * @author mahesh.jammula
 *
 */
public interface MediaInfoRepository extends JpaRepository<MediaInfo, Long>{

}
