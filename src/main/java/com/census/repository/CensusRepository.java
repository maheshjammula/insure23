package com.census.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.census.model.Census;
import org.springframework.data.jpa.repository.Query;

public interface CensusRepository extends JpaRepository<Census, Long> {
	Census findByUsername(String userName);
	
	List<Census> findByGroupId(Long groupId);

	@Query(value ="Select count(*) FROM census c JOIN Owners o ON c.group_id = o.id WHERE c.status ='Active'", nativeQuery = true)
	Long getCensusCount();
}
