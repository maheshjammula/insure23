package com.census.admin.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.census.admin.model.AdminUser;
import com.census.model.User;

public interface AdminUserRepository extends JpaRepository<AdminUser, Long> {

	AdminUser findByUsername(String username);
	List<AdminUser> findAllByOrderByIdDesc();
	AdminUser findByPwdResetCode(String pwdResetCode);

}
