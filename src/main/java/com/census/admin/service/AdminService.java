package com.census.admin.service;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.census.admin.dto.AdminUpdateEmpoyeeRequestDto;
import com.census.admin.dto.AdminUserRequestDto;
import com.census.admin.dto.DashboardDto;
import com.census.admin.dto.SubAdminResponseDto;
import com.census.admin.dto.UserListResponseDto;
import com.census.admin.model.AdminUser;
import com.census.admin.repository.AdminUserRepository;
import com.census.dto.ApiResponse;
import com.census.dto.DependentInsertRequestDto;
import com.census.dto.LoginDto;
import com.census.dto.ManagerDto;
import com.census.dto.OwnersResponseDto;
import com.census.dto.ResponseDto;
import com.census.exception.InsureException;
import com.census.model.Census;
import com.census.model.CensusDependent;
import com.census.model.Owners;
import com.census.model.Role;
import com.census.model.User;
import com.census.model.UserProfile;
import com.census.repository.CensusRepository;
import com.census.repository.DependentCensusRepository;
import com.census.repository.OwnersRepository;
import com.census.repository.RoleRespository;
import com.census.repository.SicRepository;
import com.census.repository.UserProfileRepository;
import com.census.repository.UserRepository;
import com.census.service.EmployeeService;
import com.census.service.UserService;
import com.census.utils.CommonUtil;
import com.census.utils.Constants;
import com.census.utils.DateUtils;
import com.census.utils.MessageConstant;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Service
public class AdminService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminService.class);
	
	@Autowired
	private AdminUserRepository adminUserRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OwnersRepository ownersRepository;

	@Autowired
	private CensusRepository censusRepository;

	@Autowired
	DependentCensusRepository dependentCensusRepository;

	@Autowired
	UserProfileRepository userProfileRepository;

	@Autowired
	SicRepository sicRepository;

	@Autowired
	UserService userService;

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	private CommonUtil commonUtil;
	
	@Autowired
	private RoleRespository roleRespository;
	
	public AdminUser findByUsername(String username) {
		return adminUserRepository.findByUsername(username);
	}

	public ResponseDto login(LoginDto loginDto) {
        AdminUser user = findByUsername(loginDto.getEmail());
        if(user == null) {
            throw new RuntimeException("User does not exist.");
        }
        if(new BCryptPasswordEncoder().matches(loginDto.getPassword(), user.getEncPwd())) {
        	return new ResponseDto(Constants.SUCCESS, null, MessageConstant.LOGIN_SUCCESS, null) ;
        }
		return null;
        
    }
	
	public void changeUserPassword(final AdminUser user, final String password) {
		user.setEncPwd(commonUtil.getEncodedPassword(password));
		adminUserRepository.save(user);
		LOGGER.info("Password updated successfully");
	}

	public DashboardDto getDashboard()
	{
		DashboardDto data = new DashboardDto();
		Long censuscount;
		Long brokercount = userRepository.getUserTypeCount(Constants.BROKER);
		Long employercount = userRepository.getUserTypeCount(Constants.EMPLOYER);
		Long ownerscount = ownersRepository.getOwnersCount();
		Long employeecount = censusRepository.getCensusCount();
		Long dependentcount = dependentCensusRepository.getCensusDependentCount();
		censuscount = employeecount + dependentcount;
		data.setBrokerCount(brokercount);
		data.setEmployerCount(employercount);
		data.setGroupsCount(ownerscount);
		data.setCensusCount(censuscount);

		return data;
	}

	public User addUser(AdminUserRequestDto adminUserRequestDto) throws Exception {
		User user = new User();
		UserProfile userProfile = new UserProfile();
		UUID uuid = UUID.randomUUID();
		if(adminUserRequestDto.getRole().equalsIgnoreCase("Member")){
			User manager = userRepository.findById(adminUserRequestDto.getManagerId()).get();
			Role role = roleRespository.findByRoleName("Member");
			user.setParentUser(manager);
			user.setRole(role);
			adminUserRequestDto.setAccount(manager.getAccType());
		}
		else {
			Role role = roleRespository.findByRoleName("Manager");
			user.setRole(role);
		}
		User isUserExist = userRepository.findByEmail(adminUserRequestDto.getEmail());
       	if(isUserExist == null) {
			user.setAccType(adminUserRequestDto.getAccount());
			user.setMobile(adminUserRequestDto.getPhone());
			user.setEmail(adminUserRequestDto.getEmail());
			user.setPassword(adminUserRequestDto.getPassword());
			user.setStatus(adminUserRequestDto.getAcceptance());
			user.setUsername(adminUserRequestDto.getEmail());
			user.setCreatedAt(new Date());
			user.setUpdatedAt(new Date());
			user.setUuid(uuid.toString());
			user.setEncPwd(new BCryptPasswordEncoder().encode(user.getPassword()));
			userRepository.save(user);
			if (!user.getRole().getRoleName().equalsIgnoreCase("Member") && adminUserRequestDto.getAccount().equalsIgnoreCase("Broker")) {
				userProfile.setFname(adminUserRequestDto.getUserProfile().getFirstName());
				userProfile.setLname(adminUserRequestDto.getUserProfile().getLastName());
				userProfile.setLicenseType(adminUserRequestDto.getUserProfile().getLicenseType());
				userProfile.setLicenseNo(adminUserRequestDto.getUserProfile().getLicenseNumber());
				userProfile.setLicenseExpiryDt(DateUtils.convertStringIntoDateFormate(adminUserRequestDto.getUserProfile().getLicenseExpiry()));
				userProfile.setCoStructure(adminUserRequestDto.getUserProfile().getCompanyStruct());
				userProfile.setAgencyName(adminUserRequestDto.getUserProfile().getAgencyName());
				userProfile.setAddress(adminUserRequestDto.getUserProfile().getAddress());
				userProfile.setZipcode(adminUserRequestDto.getUserProfile().getZipcode());
				userProfile.setState(adminUserRequestDto.getUserProfile().getState());
				userProfile.setCity(adminUserRequestDto.getUserProfile().getCity());
				userProfile.setStatus("Active");
				userProfile.setUser(user);

				userProfileRepository.save(userProfile);
			}
		}
       	else
		{
			throw new Exception( "User already exist.");
		}
		return user;
	}

	public Census updateEmployee(AdminUpdateEmpoyeeRequestDto adminUpdateEmpoyeeRequestDto) throws Exception {

		Census census = new Census();
		CensusDependent censusDependent = new CensusDependent();
		try {
			census = censusRepository.getOne(adminUpdateEmpoyeeRequestDto.getCensusId());
			if (census != null) {
				census.setFname(adminUpdateEmpoyeeRequestDto.getFirstName());
				census.setLname(adminUpdateEmpoyeeRequestDto.getLastName());
				census.setGender(adminUpdateEmpoyeeRequestDto.getGender());
				census.setAge(adminUpdateEmpoyeeRequestDto.getAge());
				getAgeCal(census, adminUpdateEmpoyeeRequestDto.getDob());
//				census.setDob(DateUtils.convertStringIntoDateFormate(adminUpdateEmpoyeeRequestDto.getDob()));
				census.setCounty(adminUpdateEmpoyeeRequestDto.getCountry());
				census.setZipcode(adminUpdateEmpoyeeRequestDto.getZipcode());
				census.setSpouseFlag(adminUpdateEmpoyeeRequestDto.getSpouse());
				census.setCobraFlag(adminUpdateEmpoyeeRequestDto.getCobra());
				census.setChildFlag(adminUpdateEmpoyeeRequestDto.getChild());

				censusRepository.save(census);

				if (null != adminUpdateEmpoyeeRequestDto.getDependents()
						&& adminUpdateEmpoyeeRequestDto.getDependents().size() > 0) {
					List<DependentInsertRequestDto> dependents = adminUpdateEmpoyeeRequestDto.getDependents();
					for (DependentInsertRequestDto depDto : dependents) {
						if(depDto.getId() == null) {
							depDto.setId(census.getId());
							employeeService.addDependent(depDto);
						}
						else
						{
							censusDependent = dependentCensusRepository.getOne(depDto.getId());
							if( censusDependent != null )
							{
								censusDependent.setFname(depDto.getFirstName());
								censusDependent.setLname(depDto.getLastName());
								censusDependent.setGender(depDto.getGender());
								censusDependent.setAge(depDto.getAge());
								getAgeCal(censusDependent, depDto.getDob());
//								censusDependent.setDob(DateUtils.convertStringIntoDateFormate(depDto.getDob()));
								censusDependent.setRelation(depDto.getType());

								dependentCensusRepository.save(censusDependent);
							}
						}

					}
				}
				if(adminUpdateEmpoyeeRequestDto.getChild().equalsIgnoreCase("false") )
				{
					List<CensusDependent> data = dependentCensusRepository.findByCensusAndRelation(census.getId(),"dependent");
					for( CensusDependent id : data )
					{
						dependentCensusRepository.deleteById(id.getId());
					}
				}
				if(adminUpdateEmpoyeeRequestDto.getSpouse().equalsIgnoreCase("false") )
				{
					List<CensusDependent> data = dependentCensusRepository.findByCensusAndRelation(census.getId(),"spouse");
					for( CensusDependent id : data )
					{
						dependentCensusRepository.deleteById(id.getId());
					}
				}
				LOGGER.info("Employee with id: {} is updated", census.getId());
			}

		} catch (Exception e) {
			LOGGER.error("Employee id does Not exist.");
			throw e;
		}
		return census;
	}
	
	//dependent age cal
		private void getAgeCal(CensusDependent census, String age) throws ParseException {
			if(!StringUtils.isEmpty(age) && !StringUtils.isEmpty(age.trim())){
				census.setDob(DateUtils.convertStringIntoDateFormate(age));
				LocalDate birthDate = LocalDate.parse(age);
				int calculateAge = DateUtils.calculateAge(birthDate);
				census.setAge(String.valueOf(calculateAge));
			}
		}
	
	private void getAgeCal(Census census, String dob) throws ParseException {
		if(!StringUtils.isEmpty(dob) && !StringUtils.isEmpty(dob.trim())){
			census.setDob(DateUtils.convertStringIntoDateFormate(dob));
			LocalDate birthDate = LocalDate.parse(dob);
			int calculateAge = DateUtils.calculateAge(birthDate);
			census.setAge(String.valueOf(calculateAge));
		}
	}


	public List<UserListResponseDto> findAll() {
		List<UserListResponseDto> userResponse = new ArrayList<>();
		List<User> findAllByOrderByIdDesc = userRepository.findAllByOrderByIdDesc();
		List<User> usersList = findAllByOrderByIdDesc.stream().filter(a -> a.getParentUser() == null && !a.getAccType().equalsIgnoreCase(Constants.ADMIN) && !a.getStatus().equalsIgnoreCase(Constants.STATUS_DELETED)).collect(Collectors.toList());
		
		for( User user : usersList )
		{
			UserListResponseDto response = new UserListResponseDto();
			UserProfile userProfile = userProfileRepository.findByUserId(user.getId());
			if (userProfile != null) {
				response.setfName(userProfile.getFname());
				response.setlName(userProfile.getLname());
			}
			response.setId(user.getId());
			response.setUuid(user.getUuid());
			response.setUsername(user.getUsername());
			response.setRole(user.getRole().getRoleName());
			response.setEmail(user.getEmail());
			response.setMobile(user.getMobile());
			response.setAccType(user.getAccType());
			response.setStatus(user.getStatus());
			response.setCreatedAt(user.getCreatedAt());
			response.setUpdatedAt(user.getUpdatedAt());
			userResponse.add(response);
			
		}
		return userResponse;
	}
	public List<OwnersResponseDto> findAllGroups() {
		List<Owners> findAllByOrderByIdDesc = ownersRepository.findAllByOrderByIdDesc();
		List<Owners> groupList = findAllByOrderByIdDesc.stream().filter(a-> a.getStatus() != null && !a.getStatus().equalsIgnoreCase("Deleted")).collect(Collectors.toList());
		List<OwnersResponseDto> ownersData = new ArrayList<OwnersResponseDto>();
		for (Owners data : groupList){
			OwnersResponseDto response = new OwnersResponseDto();
			response.setId(data.getId());
			response.setName(data.getGroupName());
			if (data.getSic() != null) {
				response.setSicCode(data.getSic().getSicCode());
				response.setSicName(data.getSic().getSicName());
			}
			response.setCountry(data.getCounty());
			response.setEmployees(data.getEmpCount());
			response.setZip(data.getZipcode());
			response.setAccType(data.getAccountType());
			response.setStatus(data.getStatus());
			ownersData.add(response);
	  }
		LOGGER.info("Returned the list of owners");
		return ownersData;
	}

	public List<ManagerDto> getAllManagers() {
		List<ManagerDto> userResponse = new ArrayList<>();
		List<User> findAllByOrderByIdDesc = userRepository.findAllByOrderByIdDesc();
		List<User> usersList = findAllByOrderByIdDesc.stream().filter(a -> !a.getAccType().equalsIgnoreCase(Constants.ADMIN) && null != a.getRole() && a.getRole().getRoleName().equalsIgnoreCase("Manager")).collect(Collectors.toList());
		for( User user : usersList )
		{
			ManagerDto response = new ManagerDto();
			response.setManagerId(user.getId());
			response.setUserName(user.getUsername());
			response.setEmail(user.getEmail());
			userResponse.add(response);
//			
		}
		return userResponse;
	}
	
	public List<UserListResponseDto> getAllDeletedUser() {
		List<UserListResponseDto> userResponse = new ArrayList<>();
		List<User> findAllByOrderByIdDesc = userRepository.findAllByOrderByIdDesc();
		List<User> usersList = findAllByOrderByIdDesc.stream().filter(a -> !a.getAccType().equalsIgnoreCase(Constants.ADMIN) && a.getStatus().equalsIgnoreCase(Constants.STATUS_DELETED)).collect(Collectors.toList());	
		for( User user : usersList )
		{
			UserListResponseDto response = new UserListResponseDto();
			response.setId(user.getId());
			response.setUuid(user.getUuid());
			response.setUsername(user.getUsername());
			response.setRole(user.getRole().getRoleName());
			response.setEmail(user.getEmail());
			response.setMobile(user.getMobile());
			response.setAccType(user.getAccType());
			response.setStatus(user.getStatus());
			response.setCreatedAt(user.getCreatedAt());
			response.setUpdatedAt(user.getUpdatedAt());
			userResponse.add(response);
		}
		return userResponse;
	}
	
	public List<SubAdminResponseDto> findAllAdmin() {
		List<SubAdminResponseDto> userResponse = new ArrayList<>();
		List<AdminUser> findall = adminUserRepository.findAllByOrderByIdDesc();
		List<AdminUser> finadAdmin =findall.stream().filter(a -> a.getAccType().equalsIgnoreCase(Constants.ADMIN) && !a.getStatus().equalsIgnoreCase(Constants.STATUS_DELETED)).collect(Collectors.toList());	
		
		for( AdminUser user : finadAdmin )
		{
			SubAdminResponseDto response = new SubAdminResponseDto();
			response.setId(user.getId());
			response.setUsername(user.getUsername());
			response.setAccType(user.getAccType());
			response.setStatus(user.getStatus());
			response.setCreatedAt(user.getCreatedAt());
			response.setUpdatedAt(user.getUpdatedAt());
			userResponse.add(response);
			
		}
		return userResponse;
	}

	public ApiResponse adminLogin(LoginDto loginDto) throws InsureException {
		AdminUser adminUser = adminUserRepository.findByUsername(loginDto.getEmail());
		if (adminUser == null) {
			throw new RuntimeException("account does not exist.");
		}
		if (adminUser.getStatus().equalsIgnoreCase(Constants.STATUS_INACTIVE)) {
			throw new InsureException("Your account is inactive. Contact administrator! ");
		}
		if (adminUser.getStatus().equalsIgnoreCase(Constants.STATUS_DELETED)) {
			throw new InsureException("account account is deleted.");
		}
		if (new BCryptPasswordEncoder().matches(loginDto.getPassword(), adminUser.getEncPwd())) {
			return new ApiResponse(Constants.SUCCESS, MessageConstant.LOGIN_SUCCESS, null, adminUser.getAccType(),
					false);
		}

		return null;

	}
}
