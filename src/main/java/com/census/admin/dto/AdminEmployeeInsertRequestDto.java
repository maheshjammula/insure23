package com.census.admin.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class AdminEmployeeInsertRequestDto {

    private String firstName;
    private String lastName;
    private String gender;
    private String dob;
    private Integer zipcode;
    private String country;
    private String cobra;
    private String spouse;
    private String child;
    private Long groupId;
    private List<AdminDependentInsertRequestDto> dependents = new ArrayList<>();

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public Integer getZipcode() {
        return zipcode;
    }

    public void setZipcode(Integer zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCobra() {
        return cobra;
    }

    public void setCobra(String cobra) {
        this.cobra = cobra;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public List<AdminDependentInsertRequestDto> getDependents() {
        return dependents;
    }

    public void setDependents(List<AdminDependentInsertRequestDto> dependents) {
        this.dependents = dependents;
    }
}
