package com.census.admin.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.census.dto.UserProfileRequestDto;

@Component
public class AdminUserRequestDto {
    @NotNull
    private String email;

    @NotNull
    private String password;


    private String conf_password;

    private Long phone;

    @NotNull
    @Size(min=2, message = "Account type should not empty")
    private String account;

    private String acceptance;
    
    private String role;
    
    private long managerId;

    private UserProfileRequestDto userProfile;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConf_password() {
        return conf_password;
    }

    public void setConf_password(String conf_password) {
        this.conf_password = conf_password;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAcceptance() {
        return acceptance;
    }

    public void setAcceptance(String acceptance) {
        this.acceptance = acceptance;
    }

    public UserProfileRequestDto getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfileRequestDto userProfile) {
        this.userProfile = userProfile;
    }

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public long getManagerId() {
		return managerId;
	}

	public void setManagerId(long managerId) {
		this.managerId = managerId;
	}
    
    
}
