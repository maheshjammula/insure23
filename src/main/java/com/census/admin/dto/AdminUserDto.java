package com.census.admin.dto;

import javax.validation.constraints.NotNull;

public class AdminUserDto {
	
	@NotNull
	private String email;
	
	@NotNull
	private String password;
	
	private String conf_password;
	
	private String status;
	
	private String accType;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConf_password() {
		return conf_password;
	}

	public void setConf_password(String conf_password) {
		this.conf_password = conf_password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}
	
}
