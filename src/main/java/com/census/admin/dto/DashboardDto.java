package com.census.admin.dto;

public class DashboardDto {

    private Long brokerCount;
    private Long employerCount;
    private Long groupsCount;
    private Long censusCount;
  
    public Long getGroupsCount() {
        return groupsCount;
    }

    public void setGroupsCount(Long groupsCount) {
        this.groupsCount = groupsCount;
    }

    public Long getCensusCount() {
        return censusCount;
    }

    public void setCensusCount(Long censusCount) {
        this.censusCount = censusCount;
    }

	public Long getBrokerCount() {
		return brokerCount;
	}

	public void setBrokerCount(Long brokerCount) {
		this.brokerCount = brokerCount;
	}

	public Long getEmployerCount() {
		return employerCount;
	}

	public void setEmployerCount(Long employerCount) {
		this.employerCount = employerCount;
	}

   
}
