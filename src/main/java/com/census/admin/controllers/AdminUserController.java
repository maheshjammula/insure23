package com.census.admin.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.census.admin.dto.AdminUpdateEmpoyeeRequestDto;
import com.census.admin.dto.AdminUserDto;
import com.census.admin.dto.AdminUserRequestDto;
import com.census.admin.dto.DashboardDto;
import com.census.admin.dto.SubAdminResponseDto;
import com.census.admin.dto.UpdateSubAdminDtoRequest;
import com.census.admin.dto.UpdateSubAdminProfileDto;
import com.census.admin.dto.UserListResponseDto;
import com.census.admin.model.AdminUser;
import com.census.admin.repository.AdminUserRepository;
import com.census.admin.service.AdminService;
import com.census.config.JwtTokenUtil;
import com.census.controllers.EmployeeController;
import com.census.controllers.OwnersController;
import com.census.controllers.UserController;
import com.census.dto.ApiResponse;
import com.census.dto.CensusDeleteDto;
import com.census.dto.EmployeeInsertRequestDto;
import com.census.dto.LoginDto;
import com.census.dto.ManagerDto;
import com.census.dto.OwnersRequestDto;
import com.census.dto.OwnersResponseDto;
import com.census.dto.PassUpdateDto;
import com.census.dto.ResetPasswordDto;
import com.census.dto.ResponseDto;
import com.census.dto.ShareHistoryResponseDto;
import com.census.dto.UpdateUserRequestDto;
import com.census.dto.UserDtoTransform;
import com.census.model.AppInfo;
import com.census.model.EmailTemplate;
import com.census.model.History;
import com.census.model.MediaInfo;
import com.census.model.Owners;
import com.census.model.User;
import com.census.repository.AppInfoRepository;
import com.census.repository.EmailTemplateRepository;
import com.census.repository.HistoryRepository;
import com.census.repository.MediaInfoRepository;
import com.census.repository.OwnersRepository;
import com.census.repository.UserRepository;
import com.census.service.UserService;
import com.census.utils.CommonUtil;
import com.census.utils.Constants;
import com.census.utils.EmailUtil;
import com.census.utils.MessageConstant;

import io.swagger.annotations.Api;

/**
 * 
 * @author mahesh.jammula
 *
 */
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/admin")
@Api(tags = "Admin user operations for Census data access", value = "Admin UserOperations")
public class AdminUserController {

	private static final String USERNAME_PASSWORD_WRONG = "User Name/password is wrong";


	//re-use the user controllers

	@Autowired
	private OwnersController ownersController;

	@Autowired
	private UserController userController;

	@Autowired
	private EmployeeController employeeController;

	@Autowired
	private AdminUserRepository adminUserRepository;
	

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OwnersRepository ownersRepository;

	@Autowired
	private HistoryRepository historyRepository;

	@Autowired
	private AdminService adminService;

	@Autowired
	private UserService userService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private CommonUtil commonUtil;
	
	@Autowired
	private AppInfoRepository appInfoRepository;
	
	@Autowired
	private MediaInfoRepository mediaInfoRepository;

	@Autowired
	private EmailTemplateRepository emailTemplateRepository;

	@Value("${app.baseurl}")
	private String baseurl;

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminUserController.class);

	/**
	 * Api to create admin.
	 *
	 * @return the user
	 */
	@PostMapping("/saveadmin")
	public ResponseEntity<ResponseDto> createUser(@Valid @RequestBody AdminUserDto userDto) {
		ResponseDto status = new ResponseDto();
		LOGGER.info("Calling AdminUserController::createUser");
		try {
			AdminUser adminUser = UserDtoTransform.getAdminUserFromDTO(userDto);
			adminUser.setEncPwd(commonUtil.getEncodedPassword(adminUser.getPassword()));
			adminUser = adminUserRepository.save(adminUser);
			/*if(!adminUser.getAccType().equalsIgnoreCase(Constants.SUPER_ADMIN))
			{		
			if (adminUser != null) {
				User user = null;
				user = userRepository.findByEmail(adminUser.getUsername());
				if(null != user) {
					user.setAccType(Constants.ADMIN);
					user.setEncPwd(adminUser.getEncPwd());
					user.setStatus(Constants.STATUS_ACTIVE);
				}
				else {
				user = new User();
				user.setAccType(Constants.ADMIN);
				user.setUsername(adminUser.getUsername());
				user.setEmail(adminUser.getUsername());
				user.setEncPwd(adminUser.getEncPwd());
				user.setStatus(Constants.STATUS_ACTIVE);
				}
				userRepository.save(user);
//				userController.activatemail(Constants.STATUS_ACTIVE, user.getUsername());
			} 	
			} */
			status.setStatus(Constants.SUCCESS);
			status.setMessage(MessageConstant.USER_REGISTERED_SUCCESSFULLY);
			return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Exception occured while register with admin record.");
			status.setStatus(Constants.FAIL);
			status.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(status, HttpStatus.OK);
		}

	}

	/**
	 * Api to Login the admin.
	 *
	 * @return the response
	 */
	@PostMapping("/login")
	public Object login(@RequestBody LoginDto loginDto) {
		LOGGER.info("Calling LoginController admin::login");
		try {
			AdminUser adminUser = adminUserRepository.findByUsername(loginDto.getEmail());
			if(null == adminUser) {
				return new ResponseDto(Constants.FAIL, null, USERNAME_PASSWORD_WRONG, null);
			}
			
			ApiResponse user = adminService.adminLogin(loginDto);
			user.setToken(jwtTokenUtil.generateToken(loginDto.getEmail()));
			return user;
		} catch (Exception e) {
			return new ResponseDto(Constants.FAIL, null, e.getMessage(), null);
		}
	}

	/**
	 * Api to Update password.
	 *
	 * @return the response
	 */
	@PostMapping("/updatePassword")
	public ResponseDto changeUserPassword(@RequestBody PassUpdateDto passDto) throws Exception {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling AdminController::changeAdminUserPassword");
		try {
		AdminUser adminUser = adminService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		//User user = userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
		if (!commonUtil.checkIfValidOldPassword(adminUser.getEncPwd(), passDto.getOldPassword())) {
			response.setStatus(Constants.FAIL);
			response.setMessage(MessageConstant.INVALID_PASSWORD);
			return response;
		}
		adminService.changeUserPassword(adminUser, passDto.getPassword());
		//userService.changeUserPassword(user, passDto.getPassword());
		response.setStatus(Constants.SUCCESS);
		response.setMessage(MessageConstant.PASSWORD_CHANGED_SUCCESSFULLY);
		
		} catch (Exception e) {
			LOGGER.error("Error in Calling AdminController::changeUserPassword:"+e.getMessage());
			return new ResponseDto(Constants.FAIL, null, e.getMessage(), null);
		}
		return response;
	}
	
	/**
	 * Api to Update password.
	 *
	 * @return the response
	 */
	@PostMapping("/updateUserPassword")
	public ResponseDto updateUserPassword(@RequestBody PassUpdateDto loginDto) throws Exception {
		ResponseDto response = new ResponseDto();
		try {
		LOGGER.info("Calling AdminController::updateUserPassword");
		User user = userRepository.findById(loginDto.getUserId()).get();
		if(user == null)
		{
			return new ResponseDto(Constants.SUCCESS, null, MessageConstant.USERNAME_PASSWORD_WRONG, null);
		}
		userService.changeUserPassword(user, loginDto.getPassword());
		response.setStatus(Constants.SUCCESS);
		response.setMessage(MessageConstant.PASSWORD_CHANGED_SUCCESSFULLY);
		} catch (Exception e) {
			LOGGER.error("Error in Calling AdminController::updateUserPassword:"+e.getMessage());
			return new ResponseDto(Constants.FAIL, null, e.getMessage(), null);
		}
		return response;
	}

	//imitating user screen coontrollers

	/**
	 * Api to create group.
	 *
	 * @return the response
	 */
	@PostMapping("/addGroup")
	public ResponseEntity<ResponseDto> addGroup(@RequestBody OwnersRequestDto ownersRequestDto) {
		return ownersController.addGroup(ownersRequestDto);
	}

	/**
	 * Api to edit group.
	 *
	 * @return the response
	 */
	@PostMapping("/editGroup")
	public ResponseEntity<ResponseDto> editGroup(@RequestBody OwnersRequestDto ownersRequestDto) {
		return ownersController.editGroup(ownersRequestDto);
	}

	/**
	 * Api to get group details by id.
	 *
	 * @return the response
	 */
	@GetMapping("/groupById")
	public ResponseEntity<ResponseDto> groupById(@RequestParam Long id) {
		return ownersController.getGroupsInfo(id);
	}

	/**
	 * Api to all groups.
	 *
	 * @return the response
	 */
	@GetMapping("/groups")
	public ResponseEntity<ResponseDto> getAllGroups() {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling getAllGroups");
		try
		{
			List<OwnersResponseDto> findAll = adminService.findAllGroups();
			if( findAll != null )
			{
				response.setStatus(Constants.SUCCESS);
				response.setData(findAll);

			} else
			{
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(findAll);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while getting groups"+e.getMessage());
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by AdminController:getAllGroups - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Api to delete group.
	 *
	 * @return the response
	 */
	@PostMapping("/deleteGroups")
	public ResponseEntity<ResponseDto> deleteGroups(@RequestParam("id") Long groupId) {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling AdminUserController::deleteGroups");
		try {
			Owners owners = ownersRepository.getOne(groupId);
			if (owners != null) {
				owners.setStatus("Deleted");
				owners.setGroupName(owners.getGroupName()+"_"+"deleted");
				ownersRepository.save(owners);
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.DELETE_RECORD);
			}
			else
			{
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.PLEASE_CONSTACT_TO_ADMINISTRATOR);	
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting group record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Api to create user.
	 *
	 * @return the response
	 */
	@PostMapping("/users")
	public ResponseEntity<ResponseDto> createUser(@Valid @RequestBody AdminUserRequestDto adminUserRequestDto) {
		ResponseDto response  = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling UserController::createUser");
		try {
			User user = adminService.addUser(adminUserRequestDto);
			if (user != null && user.getAccType().equalsIgnoreCase(Constants.BROKER)) {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.USER_REGISTERED_SUCCESSFULLY);
				response.setData(user.getId());
			} else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.USER_REGISTERED_SUCCESSFULLY);
			}
		}catch (Exception e) {
			LOGGER.error("Exception occured while creating user");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by UserController:createUser - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}



	/**
	 * Get all users.
	 *
	 * @return the User
	 */
	@GetMapping("/users/getProfile")
	public ResponseEntity<ResponseDto> getUserProfile(@RequestParam("userName") String userName) {
		if (StringUtils.isBlank(userName)) {
			return new ResponseEntity<>(new ResponseDto(Constants.FAIL, null, MessageConstant.USER_NAME_REQUIRED, null), HttpStatus.OK);
		}
		return userController.getUserProfile(userName);
	}

	/**
	 * Api to update the UserProfile.
	 *
	 * @return the response
	 */
	@PostMapping("/users/updateProfile")
	public ResponseEntity<ResponseDto> updateUserProfile(@RequestBody UpdateUserRequestDto updateUserRequestDto) {
		ResponseDto response = new ResponseDto();
		if (StringUtils.isBlank(updateUserRequestDto.getUserName())) {
			return new ResponseEntity<>(new ResponseDto(Constants.FAIL, null, MessageConstant.USER_NAME_REQUIRED, null), HttpStatus.OK);
		}
		userController.updateUserProfile(updateUserRequestDto,true);
		response.setStatus(Constants.SUCCESS);
		response.setMessage(MessageConstant.UPDATED_SUCCESSFULLY);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Api to delete the user.
	 *
	 * @return the response
	 */
	@PostMapping("/users/delete")
	public ResponseEntity<ResponseDto> deleteUser(@RequestParam("id") Long userId) {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling AdminUserController::deleteUser");
		try {
			  User user = userRepository.getOne(userId);
			  if(user != null)
			  {
				  user.setStatus(Constants.STATUS_DELETED);
				  userRepository.save(user);  
			  }
			response.setStatus(Constants.SUCCESS);
			response.setMessage(MessageConstant.DELETE_RECORD);

		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting user record");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by AdminController:deleteUser - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Get all users list.
	 *
	 * @return the list
	 */
	@GetMapping("/userslist")
	public ResponseEntity<ResponseDto> getAllUser() {
		ResponseDto response = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling getAllUsers");
		try{
			List<UserListResponseDto> findAll = adminService.findAll();
			if(findAll != null )
			{
				response.setStatus(Constants.SUCCESS);
				response.setData(findAll);
			}
			else
			{
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(findAll);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while getting user record");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by AdminController:getAllUser - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Get all users list.
	 *
	 * @return the list
	 */
	@GetMapping("/managerlist")
	public ResponseEntity<ResponseDto> getAllManagers() {
		LOGGER.info("Calling getAllManagers");
		List<ManagerDto> findAll = adminService.getAllManagers();
		if (findAll == null) {
			return new ResponseEntity<>(new ResponseDto(Constants.FAIL, null,"No Managers found!", null),HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseDto(Constants.SUCCESS, findAll, null, null),HttpStatus.OK);
		}
	}

	/**
	 * Api to delete the employee.
	 *
	 * @return the response
	 */
	@PostMapping("/employee/delete")
	public ResponseEntity<ResponseDto> deleteEmployee(@RequestBody CensusDeleteDto censusDeleteDto) {
		return employeeController.deleteEmployee(censusDeleteDto);
	}

	/**
	 * Api to delete the dependent.
	 *
	 * @return the response
	 */
	@PostMapping("/delete/dependent")
	public ResponseEntity<ResponseDto> deleteDependent(@RequestBody CensusDeleteDto censusDeleteDto) {
		return employeeController.deleteDependent(censusDeleteDto);
	}

	/**
	 * Get all employee list.
	 *
	 * @return the list
	 */
	@GetMapping("/employeelist")
	public ResponseEntity<ResponseDto> getAllEmployees(@RequestParam(value = "groupId", required=false) String groupId) {
		return employeeController.getEmployee(groupId);
	}

	/**
	 * Get all employee list.
	 *
	 * @return the list
	 */
	@GetMapping("/employeeById")
	public ResponseEntity<ResponseDto> getEmployeesById(@RequestParam(value = "id") Long id) {
		return employeeController.getEmployeeInfoById(id);
	}
	/**
	 * Api to add the employee record.
	 *
	 * @return the response
	 */
	@PostMapping("/addemployee")
	public ResponseEntity<ResponseDto> addEmployee(@RequestParam("group_id") Long groupId,@RequestBody List<EmployeeInsertRequestDto> employeeInsertRequestDto) {
		/*if (StringUtils.isBlank(employeeInsertRequestDto.getUserName())) {
			return new ResponseEntity<>(new ResponseDto(Constants.FAIL, null, MessageConstant.USER_NAME_REQUIRED, null), HttpStatus.OK);
		}*/
		return employeeController.addEmployee(groupId,employeeInsertRequestDto);
	}

	/**
	 * Api to updated the employee record.
	 *
	 * @return the response
	 */
	@PostMapping("/updateemployee")
	public ResponseEntity<ResponseDto> updateEmployee(@RequestBody AdminUpdateEmpoyeeRequestDto adminUpdateEmpoyeeRequestDto) {
		ResponseDto response  = new ResponseDto();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling EmployeeController::updateEmployee");
		try{
			adminService.updateEmployee(adminUpdateEmpoyeeRequestDto);
			response.setStatus(Constants.SUCCESS);
			response.setMessage(MessageConstant.UPDATED_SUCCESSFULLY);

		} catch(Exception e)
		{
			LOGGER.error("Exception occured while updating employee records.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response,HttpStatus.OK);

		}
		LOGGER.debug("Time taken by EmployeeController:updateEmployee - {}",
				System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response,HttpStatus.OK);
	}

	/**
	 * Get dashboard result.
	 *
	 * @return the response
	 */
	@GetMapping("/getdashboard")
	public ResponseEntity<ResponseDto> getDashboard() {
		DashboardDto data = adminService.getDashboard();
		if ( data == null ) {
			return new ResponseEntity<>(new ResponseDto(Constants.FAIL, null, null, null), HttpStatus.OK);
		}
		return new ResponseEntity<>(new ResponseDto(Constants.SUCCESS, data, null, null), HttpStatus.OK);
	}
	
	/**
	 * Api to share the groups with others users.
	 *
	 *
	 * @return the response
	 */
	@PostMapping("/shareGroupHistory")
	public ResponseEntity<ResponseDto> shareGroupHistory() {
		ResponseDto response = new ResponseDto();
		List<ShareHistoryResponseDto> historyData = new ArrayList<>();
		long startTime = System.currentTimeMillis();
		LOGGER.info("Calling shareGroupHistory::shareGroupHistory");
		try {
			List<History> history = historyRepository.findAllByOrderByIdDesc();
			List<History> historyList = history.stream().filter(a-> a.getStatus().equalsIgnoreCase("Active")).collect(Collectors.toList());
			if (historyList != null)
			{
				for (History list : historyList) {
					ShareHistoryResponseDto shareData = new ShareHistoryResponseDto();
					String groupname = ownersRepository.findByGroupId(list.getGroupId());
					shareData.setId(list.getId());
					shareData.setSharedBy(list.getSharedBy());
					shareData.setSharedTo(list.getSharedTo());
					shareData.setGroupId(list.getGroupId());
					shareData.setGroupName(groupname);
					shareData.setStatus(list.getStatus());
					shareData.setCreatedAt(list.getCreatedAt());
					shareData.setUpdatedAt(list.getUpdatedAt());
					historyData.add(shareData);
				}
				response.setStatus(Constants.SUCCESS);
				response.setData(historyData);
			}
			else {
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.NO_DATA_FOUND);
				response.setData(historyList);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching group history");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		LOGGER.debug("Time taken by shareGroupHistory - {}", System.currentTimeMillis() - startTime);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Api to delete the share group with other users.
	 *
	 * @return the list
	 */
	@PostMapping("/shareHistoryDelete")
	public ResponseEntity<ResponseDto> shareGroupDelete(@RequestParam("historyId") Long historyId){
		return ownersController.shareGroupDelete(historyId);
	}
	
	
	@GetMapping("/appinfo")
	public ResponseEntity<ResponseDto> appinfo(){
		ResponseDto response = new ResponseDto();
		List<AppInfo> appInfo = appInfoRepository.findAll();
		response.setStatus(Constants.SUCCESS);
		response.setData((appInfo.get(0)));
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/saveAppinfo")
	public ResponseEntity<ResponseDto> saveAppinfo(@RequestBody AppInfo appInfo){
		ResponseDto response = new ResponseDto();
		List<AppInfo> appInfos = appInfoRepository.findAll();
		if(null != appInfos & appInfos.size() > 0) {
			AppInfo existAppInfo = appInfos.get(0);
			appInfo.setId(existAppInfo.getId());
		}
		appInfoRepository.save(appInfo);
		response.setStatus(Constants.SUCCESS);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	
	
	@GetMapping("/mediainfo")
	public ResponseEntity<ResponseDto> mediainfo(){
		ResponseDto response = new ResponseDto();
		List<MediaInfo> mediaInfo = mediaInfoRepository.findAll();
		response.setStatus(Constants.SUCCESS);
		response.setData(mediaInfo);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@Transactional
	@PostMapping("/saveMediainfo")
	public ResponseEntity<ResponseDto> saveMediainfo(@RequestBody List<MediaInfo> mediaInfo){
		ResponseDto response = new ResponseDto();
		try {
			mediaInfo.forEach(mediainf ->{
				mediaInfoRepository.save(mediainf);
			});
			response.setStatus(Constants.SUCCESS);
		} 
		catch (DataIntegrityViolationException e) {
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getCause().getCause().getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		catch (Exception es) {
			LOGGER.error("Exception occured while fetching group history"+es.getMessage());
			response.setStatus(Constants.FAIL);
			response.setMessage(es.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/getGroupStatus")
	public ResponseEntity<ResponseDto> getGroupStatus(){
		ResponseDto response = new ResponseDto();
		response.setStatus(Constants.SUCCESS);
		List<String> statuses = Arrays.asList("Progress,Complete,Inactive");
		response.setData(statuses);
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/getRecycleBin")
	public ResponseEntity<ResponseDto> getRecycleBinData() {
		ResponseDto response = new ResponseDto();
		try {
			List<UserListResponseDto> findAll = adminService.getAllDeletedUser();
			if (findAll != null) {
				response.setStatus(Constants.SUCCESS);
				response.setData(findAll);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching recyclebin data" + e.getMessage());
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/deletepermanently")
	public ResponseEntity<ResponseDto> deleteRecycleBinData(@RequestParam("userId") Long userId) {
		ResponseDto response = new ResponseDto();
		try {
			User deleteUserId = userRepository.getOne(userId);
			if(deleteUserId != null) {
				List<User> users = userRepository.getAllUserByParentId(deleteUserId.getId());
				if(users != null)
				{
					for(User user : users)
					{
						userRepository.deleteById(user.getId());
					}
				}
				userRepository.deleteById(userId);
				response.setStatus(Constants.SUCCESS);
				response.setMessage(MessageConstant.DELETE_RECORD);
			}

		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting user record" + e.getMessage());
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@PostMapping("/restoreuser")
	public ResponseEntity<ResponseDto> restoreRecycleBinData(@RequestParam("userId") Long userId) {
		ResponseDto response = new ResponseDto();
		try {
			User user = userRepository.getOne(userId);
			if (user != null) {
				user.setStatus(Constants.STATUS_ACTIVE);
				userRepository.save(user);
			}
			response.setStatus(Constants.SUCCESS);
			response.setMessage(MessageConstant.RESTORED);

		} catch (Exception e) {
			LOGGER.error("Exception occured while restore the user record" + e.getMessage());
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<>(response, HttpStatus.OK);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	@GetMapping("/getsubadmin")
	public ResponseEntity<ResponseDto> getAdmin() {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling AdminUserController::getAdmin");
		try {
			List<SubAdminResponseDto> findall = adminService.findAllAdmin();
			if(findall != null)
			{
			response.setStatus(Constants.SUCCESS);
			response.setData(findall);
			}
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Exception occured while geting with admin record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		}

	}
	
	@PostMapping("/deletesubadmin")
	public ResponseEntity<ResponseDto> deleteAdmin(@RequestParam("id") Long userId) {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling AdminUserController::getAdmin");
		try {
			//User user = userRepository.getOne(userId);
			AdminUser adminuser = adminUserRepository.getOne(userId);
			if (adminuser != null)
			{
                adminUserRepository.deleteById(adminuser.getId());
                //userRepository.deleteById(userId);
                response.setMessage(MessageConstant.DELETE_RECORD);
				response.setStatus(Constants.SUCCESS);
			}
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting with admin record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		}

	}	
	
	@GetMapping("/getSubAdminById")
	public ResponseEntity<ResponseDto> getAdminById(@RequestParam("id") Long id) {
		ResponseDto response = new ResponseDto();
		SubAdminResponseDto dto = new SubAdminResponseDto();
		LOGGER.info("Calling AdminUserController::getAdmin");
		try {
			AdminUser adminuser = adminUserRepository.getOne(id);
			if (adminuser != null)
			{
				dto.setId(adminuser.getId());
				dto.setUsername(adminuser.getUsername());
				dto.setAccType(adminuser.getAccType());
				dto.setStatus(adminuser.getStatus());
				dto.setCreatedAt(adminuser.getCreatedAt());
				dto.setUpdatedAt(adminuser.getUpdatedAt());
				response.setData(dto);
				response.setStatus(Constants.SUCCESS);
			}
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Exception occured while deleting with admin record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		}

	}	
	
	@PostMapping("/updateSubAdmin")
	public ResponseEntity<ResponseDto> updateSubAdmin(@RequestBody UpdateSubAdminProfileDto  updateSubAdminProfileDto) {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling AdminUserController::updateSubAdmin");
		try {
			//User user = userRepository.getOne(updateSubAdminProfileDto.getId());
			AdminUser adminuser = adminUserRepository.getOne(updateSubAdminProfileDto.getId());
			if (adminuser != null)
			{
				adminuser.setUsername(updateSubAdminProfileDto.getEmail());
				adminuser.setStatus(updateSubAdminProfileDto.getStatus());
				adminUserRepository.save(adminuser);
				response.setStatus(Constants.SUCCESS);
			}
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Exception occured while update subadmin record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		}

	}	
	
	@PostMapping("/updateSubAdminPassword")
	public ResponseEntity<ResponseDto> updateSubAdminpassword(@RequestBody UpdateSubAdminDtoRequest updateSubAdminDtoRequest) {
		ResponseDto response = new ResponseDto();
		LOGGER.info("Calling AdminUserController::updateSubAdminpassword");
		try {
			//User user = userRepository.getOne(updateSubAdminDtoRequest.getId());
			AdminUser adminuser = adminUserRepository.getOne(updateSubAdminDtoRequest.getId());
			if (adminuser != null)
			{
				adminuser.setEncPwd(commonUtil.getEncodedPassword(updateSubAdminDtoRequest.getPassword()));
				adminUserRepository.save(adminuser);
				response.setStatus(Constants.SUCCESS);
			}
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Exception occured while updating with admin record.");
			response.setStatus(Constants.FAIL);
			response.setMessage(e.getMessage());
			return new ResponseEntity<ResponseDto>(response, HttpStatus.OK);
		}

	}	

	/**
	 * Api to forgot password.
	 *
	 *
	 * @return the response
	 * @throws Exception
	 */

	@RequestMapping(value = "/adminforgot", method = RequestMethod.POST)
	public ResponseDto processForgotPasswordForm(@RequestParam("email") String userEmail, HttpServletRequest request)
			throws Exception {
		AdminUser adminuser = adminService.findByUsername(userEmail);
		LOGGER.info("The forgor password processing started with baseurl:" + baseurl);

		if (adminuser == null) {
			return new ResponseDto(Constants.FAIL, null, MessageConstant.EMAIL_DOES_NOT_EXIST, null);
		} else {
			UUID uuid = UUID.randomUUID();
			adminuser.setPwdResetCode(uuid.toString());
			adminUserRepository.save(adminuser);
			EmailTemplate email = emailTemplateRepository.findByTemplateId("forget_password");
			if (email != null) {
				EmailUtil.sendMail(adminuser.getUsername(), email.getSubject(),
						email.getMailBody() + baseurl + "/admin/reset-password?token=" + adminuser.getPwdResetCode());
			}
		}
		return new ResponseDto(Constants.SUCCESS, null, MessageConstant.EMAIL_SENT, null);

	}

	/**
	 * Api to reset password.
	 *
	 *
	 * @return the response
	 * @throws Exception
	 */

	@RequestMapping(value = "/adminreset", method = RequestMethod.POST)
	public ResponseDto resetCustomerPassword(@RequestBody ResetPasswordDto resetPassword) throws Exception {
		LOGGER.info("Calling LoginController::resetCustomerPassword");
		AdminUser adminuser = adminUserRepository.findByPwdResetCode(resetPassword.getToken());
		if (adminuser == null) {
			return new ResponseDto(Constants.FAIL, null, MessageConstant.INVALID_RESET_LINK, null);
		}
		adminService.changeUserPassword(adminuser, resetPassword.getNewpassword());
		LOGGER.info("Update password completed for user :" + adminuser.getUsername());
		adminuser.setPassword(resetPassword.getNewpassword());
		EmailTemplate email = emailTemplateRepository.findByTemplateId("reset_password");
		if (email != null) {
			EmailUtil.sendMail(adminuser.getUsername(), email.getSubject(), email.getMailBody());
		}
		// updatePasswordMail(user);
		return new ResponseDto(Constants.SUCCESS, null, MessageConstant.RESET_PASSWORD_SUCCESSFULLY, null);
	}

}
