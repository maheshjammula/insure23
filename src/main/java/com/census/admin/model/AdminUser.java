package com.census.admin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Entity
@Table(name = "admin_users")
@EntityListeners(AuditingEntityListener.class)
public class AdminUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column(name = "uuid")
    private String uuid;
    
    @Column(name = "uname")
    private String username;
    
    @Column(name = "enc_pwd")
    private String encPwd;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "acc_type")
    private String accType	;

	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name = "pwd_reset_code")
	private String pwdResetCode;

    @Transient
    private String password;
    
    @Transient
    private String passwordConfirm;
    
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at")
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_at")
    private Date updatedAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEncPwd() {
		return encPwd;
	}

	public void setEncPwd(String encPwd) {
		this.encPwd = encPwd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getPwdResetCode() {
		return pwdResetCode;
	}

	public void setPwdResetCode(String pwdResetCode) {
		this.pwdResetCode = pwdResetCode;
	}

	@Override
	public String toString() {
		return "AdminUser [id=" + id + ", uuid=" + uuid + ", username=" + username + ", encPwd=" + encPwd + ", status="
				+ status + ", accType=" + accType + ", pwdResetCode=" + pwdResetCode + ", password=" + password
				+ ", passwordConfirm=" + passwordConfirm + ", createdAt=" + createdAt + ", updatedAt=" + updatedAt
				+ "]";
	}
}
