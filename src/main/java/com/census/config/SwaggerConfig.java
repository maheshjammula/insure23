package com.census.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * This is configuration class that sets up necessary configurations for swagger
 * documentation of end-point.
 * @author mahesh.jammula
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	public static final String AUTHORIZATION_HEADER = "Authorization";

	@Value("${swagger.title}")
	private String swaggerTitle;
	
	@Value("${swagger.description}")
	private String swaggerDesc;
	
	@Value("${swagger.terms}")
	private String swaggerTerm;
	
	@Value("${swagger.contact.name}")
	private String swaggerContactName;
	
	@Value("${swagger.contact.email}")
	private String swaggerContactEmail;
	
	@Value("${swagger.contact.url}")
	private String swaggerContactUrl;

	@Value("${app.build.number}")
	private String buildNumber;
	
	/**
	 * Enable SWAGGER 2
	 * 
	 * @return {@link Docket}
	 */
	@Bean
	public Docket newsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.groupName("Insure32 api - User")
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.census.controllers,com.census.admin.controllers"))
				.build()
				.apiInfo(apiInfo());
	}
	
	 @Bean
	    public Docket swaggerProductApi() {
	        return new Docket(DocumentationType.SWAGGER_2)
	                .groupName("Insure32 api - Admin")
	                .select()
	                .apis(RequestHandlerSelectors.basePackage("com.census.admin.controllers"))
	                .build()
					.apiInfo(apiInfo());
	    }

	/**
	 * Setting SWAGGER description for Micro Service
	 * 
	 * @return {@link ApiInfo}
	 */
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title(swaggerTitle)
				.description(swaggerDesc)
				.termsOfServiceUrl(swaggerTerm)
				.contact(new Contact(swaggerContactName, swaggerContactUrl, swaggerContactEmail))
				.version(buildNumber)
				.build();
	}
	
	@Bean
	public Docket api() {
	    return new Docket(DocumentationType.SWAGGER_2)
	            .select()
	            .apis(RequestHandlerSelectors.any())
	            .paths(PathSelectors.any())
	            .build()
	            .apiInfo(apiInfo())
	            .securitySchemes(Arrays.asList(apiKey()));
	}


	private ApiKey apiKey() {
	    return new ApiKey("jwtToken", "Authorization", "header");
	}
}
