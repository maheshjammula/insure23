package com.census.config;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.census.exception.ErrorResponse;
import com.census.utils.Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Component
public class JWTAuthorizationFilter extends OncePerRequestFilter {

	@Value("${app.server.url}")
	private List<String> EXCLUDE_URLS;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		try {
//			System.out.println(request.getRequestURI());
//			boolean isSwagger = request.getRequestURI().contains("swagger-ui") || request.getRequestURI().contains("configuration")
//					|| request.getRequestURI().contains("swagger-resources") || request.getRequestURI().contains("webjars")
//					||request.getRequestURI().contains("api-docs");
//			if (!EXCLUDE_URLS.contains(request.getRequestURI())) {
//				if(!isSwagger) {
//				checkJWTTokenForURls(request);
//				}
//			}

			if (checkJWTToken(request, response)) {
				Claims claims = validateToken(request);
//				Boolean tokenExpired = isTokenExpired(claims);
//				if(tokenExpired) throw new ExpiredJwtException(null, null, "Token expired!");
				
				if (claims.get("authorities") != null) {
					setUpSpringAuthentication(claims);
				} else {
					SecurityContextHolder.clearContext();
				}
			}
			chain.doFilter(request, response);
		} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException e) {

			// custom error response class used across my project
			ErrorResponse errorResponse = new ErrorResponse(new Date(), Constants.INVALID_TOKEN, "Token Expired",
					e.getMessage());

			response.setStatus(HttpStatus.OK.value());
			response.getWriter().write(convertObjectToJson(errorResponse));
			return;
		} catch (Exception e) {

			// custom error response class used across my project
			ErrorResponse errorResponse = new ErrorResponse(new Date(), Constants.INVALID_TOKEN, "Token not provided",
					e.getMessage());

			response.setStatus(HttpStatus.OK.value());
			response.getWriter().write(convertObjectToJson(errorResponse));
			return;
		}
	}

	private Claims validateToken(HttpServletRequest request) {
		String jwtToken = request.getHeader(Constants.HEADER).replace(Constants.PREFIX, "");
		return Jwts.parser().setSigningKey(Constants.SECRET.getBytes()).parseClaimsJws(jwtToken).getBody();
	}
	
	public <T> T getClaimFromToken(Claims claims,Function<Claims, T> claimsResolver) {
		return claimsResolver.apply(claims);
	}
	
	// check if the token has expired
	public Boolean isTokenExpired(Claims claims) {
		final Date expiration = getClaimFromToken(claims,Claims::getExpiration);
		return expiration.before(new Date());
	}

	/**
	 * Authentication method in Spring flow
	 * 
	 * @param claims
	 */
	private void setUpSpringAuthentication(Claims claims) {
		@SuppressWarnings("unchecked")
		List<String> authorities = (List<String>) claims.get("authorities");

		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(claims.getSubject(), null,
				authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
		SecurityContextHolder.getContext().setAuthentication(auth);

	}

	private boolean checkJWTToken(HttpServletRequest request, HttpServletResponse res) {
		String authenticationHeader = request.getHeader(Constants.HEADER);
//		if (authenticationHeader == null || !authenticationHeader.startsWith(Constants.PREFIX))
		if (authenticationHeader == null)
			return false;
		return true;
	}

//	private void checkJWTTokenForURls(HttpServletRequest request) throws Exception {
//		String authenticationHeader = request.getHeader(Constants.HEADER);
////		if (authenticationHeader == null || !authenticationHeader.startsWith(Constants.PREFIX))
//		if (authenticationHeader == null)
//			throw new Exception("Authorization required");
//	}

	public String convertObjectToJson(Object object) throws JsonProcessingException {
		if (object == null) {
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

}
