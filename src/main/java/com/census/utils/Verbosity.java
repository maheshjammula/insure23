package com.census.utils;

import java.util.Arrays;

/**
 * 
 * @author mahesh.jammula
 *
 */
public enum Verbosity {

	BROKER(Constants.BROKER), 
	ADMIN(Constants.ADMIN), 
	USER(Constants.USER),
	EMPLOYER(Constants.EMPLOYER);
	

	private String text;

	Verbosity(String text) {
		this.text = text;
	}

	public String getText() {
		return this.text;
	}

	public static Verbosity findByAbbr(final String abbr) {
		return Arrays.stream(values()).filter(value -> value.text.equalsIgnoreCase(abbr)).findFirst().orElse(null);
	}
};