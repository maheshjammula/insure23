package com.census.utils;

import java.util.Comparator;

import com.census.admin.dto.UserListResponseDto;

public class UserStatusSorter implements Comparator<UserListResponseDto> {

	@Override
	public int compare(UserListResponseDto u1, UserListResponseDto u2) {
		return u2.getStatus().compareToIgnoreCase(u2.getStatus());
	}

}
