package com.census.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.QuoteMode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.census.model.Census;
import com.census.model.CensusDependent;
/**
 * 
 * @author mahesh.jammula
 *
 */
public class CSVHelper {
	public static String TYPE = "text/csv";
	static String[] HEADERS = { "Census Id", "User Name", "Last Name", "First Name", "Gender", "DOB","Age", "Zip Code",
			"County", "Cobra Flag", "Child Flag", "Spouse Flag", "Quote", "Status", "Group Id","Dependent censusId","Relation" };

	static SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");

	public static boolean hasCSVFormat(MultipartFile file) {

		if (!TYPE.equals(file.getContentType())) {
			return true;
		}

		return true;
	}

	public static List<Census> csvToCensus(InputStream is, String userName, String groupId) throws NumberFormatException, ParseException {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				CSVParser csvParser = new CSVParser(fileReader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

			List<Census> censuses = new ArrayList<Census>();

			Iterable<CSVRecord> csvRecords = csvParser.getRecords();
//			S.No	Family number	Last Name	First Name	Gender	DOB	Zip Code	Country	Cobra Flag	Child Flag	Spouse Flag	Status	Relation

			Map<String,Census> excelData = new HashMap<>();
			for (CSVRecord csvRecord : csvRecords) {
				Census census = null;
				if(null == excelData.get(csvRecord.get("Family number"))) {
					census = new Census(userName, 
							csvRecord.get("Last Name"),
							csvRecord.get("First Name"), 
							csvRecord.get("Gender"), 
							StringUtils.isEmpty(csvRecord.get("DOB"))? formatter.parse(csvRecord.get("DOB")):null,
							csvRecord.get("Age"),
							Integer.valueOf(csvRecord.get("Zip Code")), 
							csvRecord.get("County"),
							csvRecord.get("Cobra Flag"), 
							csvRecord.get("Child Flag"), 
							csvRecord.get("Spouse Flag"),
							csvRecord.get("Quote"),
							csvRecord.get("Status"), 
							Long.valueOf(groupId));
					censuses.add(census);
				}
				else {
					census = excelData.get(csvRecord.get("Family number"));
					CensusDependent censusDependent = new CensusDependent();
					censusDependent.setFname(csvRecord.get("First Name"));
					censusDependent.setLname(csvRecord.get("Last Name"));
					censusDependent.setDob(StringUtils.isEmpty(csvRecord.get("DOB"))? formatter.parse(csvRecord.get("DOB")):null);
					censusDependent.setAge(csvRecord.get("Age"));
					censusDependent.setStatus(csvRecord.get("Status"));
					censusDependent.setRelation(csvRecord.get("Relation"));
					censusDependent.setGender(csvRecord.get("Gender"));
					if(census.getDependentCensus() == null) {
						census.setDependentCensus(new HashSet<CensusDependent>());
					}
					census.getDependentCensus().add(censusDependent);
				}
				
				excelData.put(csvRecord.get("Family number"), census);
				
				
			}

			return censuses;
		} catch (IOException e) {
			throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
		}
	}

	public static ByteArrayInputStream censusToCSV(List<Census> censuses) throws ParseException {
		final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

		DateFormat inputFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		DateFormat outputFormatter = new SimpleDateFormat("dd-MM-yyyy");

		try (ByteArrayOutputStream out = new ByteArrayOutputStream();
				CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {

			csvPrinter.printRecord(Arrays.asList(HEADERS));
			for (Census census : censuses) {
				Date parseDate = (null != census.getDob()) ? inputFormatter.parse(census.getDob().toString()):null;
				String formatDate = (null != parseDate)? outputFormatter.format(parseDate):null;
				List<String> data = Arrays.asList(String.valueOf(census.getId()), 
						census.getUsername(),
						census.getLname(), 
						census.getFname(), 
						census.getGender(), 
						formatDate,
						census.getAge(),
						String.valueOf(census.getZipcode()), 
						census.getCounty(), 
						census.getCobraFlag(),
						census.getChildFlag(), 
						census.getSpouseFlag(), 
						census.getQuote(),
						census.getStatus(),
						String.valueOf(census.getGroupId()));
				csvPrinter.printRecord(data);
				Set<CensusDependent> dependentCensus = census.getDependentCensus();
				for (Object censusDependent : safe(dependentCensus)) {
					CensusDependent cd = (CensusDependent) censusDependent;
					Date parseDDate = (null != cd.getDob()) ? inputFormatter.parse(cd.getDob().toString()):null;
					String formatDDate = (null != parseDDate)? outputFormatter.format(parseDDate):null;
					List<String> dependentData = Arrays.asList(String.valueOf(census.getId()), 
							"",
							cd.getLname(), 
							cd.getFname(), 
							cd.getGender(), 
							formatDDate,
							"", 
							"", 
							"",
							"", 
							"", 
							cd.getStatus(),"",
							String.valueOf(cd.getId()),
							cd.getRelation());
					csvPrinter.printRecord(dependentData);
				}

				
			}

			csvPrinter.flush();
			return new ByteArrayInputStream(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
		}
	}

	@SuppressWarnings("rawtypes")
	public static Set safe(Set other) {
		return other == null ? Collections.EMPTY_SET : other;
	}
}