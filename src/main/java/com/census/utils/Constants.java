package com.census.utils;

public class Constants {
	
	public final static String SUCCESS = "success";
	public final static String FAIL = "fail";
	public final static String INVALID_TOKEN = "invalid_token";
	
	
	//security config
	
	public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;
	public static final String SECRET = "mySecretKey";
	public static final String HEADER = "Authorization";
	public static final String PREFIX = "Bearer ";
	
	
	//common constants
	public static final String ADMIN = "admin";
	public static final String SUPER_ADMIN = "super_admin";
	public static final String USER = "user";
	public static final String BROKER = "broker";
	public static final String EMPLOYER = "employer";
	
	public static final String GROUP_TYPE_USER = "user";
	public static final String GROUP_TYPE_BROKER = "broker";
	public static final String STATUS_ACTIVE = "active";
	public static final String STATUS_INACTIVE = "inactive";
	public static final String STATUS_DELETED = "Deleted";
	

}
