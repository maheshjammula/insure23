package com.census.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;

public class DateUtils {

    //Convert Date into this formate "yyyy-MM-dd"
    public static String converUtcIntoString(String dateFormate) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = inputFormat.parse(dateFormate);
        String formattedDate = outputFormat.format(date);
        return formattedDate;
    }

    //Convert String into Date formate "yyyy-dd-mm"
    public static Date convertStringIntoDate(String dob) throws ParseException
    {
    	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    	Date date = formatter.parse(dob);
        return date;

    }
   

    public static String convertDateIntoUtc(Date dob) throws ParseException {
        Date date = new Date();
        String stringDate = "";
        SimpleDateFormat inputFormate = new SimpleDateFormat("yyyy-mm-dd");
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-dd-dd'T'HH:mm:ss.SSS'Z'");
        stringDate = inputFormate.format(dob);
        date = outputFormat.parse(stringDate);
        stringDate = outputFormat.format(date);

        return stringDate;
    }
    
    public static String convertDateToStrng(Date dob) throws ParseException
    {
    	String stringDate = "";
        DateFormat outputFormatter = new SimpleDateFormat("MM/dd/yyyy");
		stringDate = outputFormatter.format(dob);
        return stringDate;

    }

    public static Date convertStringIntoDateFormate(String dob) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(dob);
        return date;
    }

    public static String convertDateIntoStringFormate(Date dob) throws ParseException {
        String stringDate = "";
        DateFormat outputFormatter = new SimpleDateFormat("yyyy-MM-dd");
        stringDate = outputFormatter.format(dob);
        return stringDate;
    }
    
    public static int calculateAge(LocalDate birthDate) {
        if (birthDate != null) {
        	LocalDate currentDate = LocalDate.now();
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }

}
