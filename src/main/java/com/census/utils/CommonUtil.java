package com.census.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * This class used for util common operations that requires spring features
 * @author mahesh.jammula
 *
 */
@Component
public class CommonUtil {

	@Autowired
	private PasswordEncoder passwordEncoder;

	public boolean checkIfValidOldPassword(final String password, final String oldPassword) {
		return passwordEncoder.matches(oldPassword, password);
	}
	
	public String getEncodedPassword(final String password) {
		return passwordEncoder.encode(password);
	}
	
}
