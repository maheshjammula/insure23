package com.census.utils;

public class MessageConstant {

    public final static String USERPROFILE_UPDATED_SUCCESSFULLY = "UserProfile updated successfully.";
    public final static String USER_DOES_NOT_EXIST = "User does not exist.";
    public final static String USER_REGISTERED_SUCCESSFULLY = "User registered successfully.";
    public final static String GROUP_ADDED_SUCCESSFULLY = "Group information added successfully.";
    public final static String GROUP_UPDATED_SUCCESSFULLY = "Group information updated successfully.";
    public final static String GROUP_NOT_EXIST = "Groups are not exist.";
    public final static String GROUP_AND_USER_NOT_EXIST = "GroupId/userId does not exist.";
    public final static String UPDATED_SUCCESSFULLY = "Updated data successfully.";
    public final static String ID_NOT_EXIST =  "Id does not exist.";
    public final static String DELETE_RECORD = "Deleted record successfully.";
    public final static String RECORD_ADDED_SUCCESSFULLY = " Record inserted successfully.";
    public final static String INVALID_PASSWORD = "Invalid password.";
    public final static String PASSWORD_CHANGED_SUCCESSFULLY = "Password Changed successfully.";
    public final static String LOGOUT_SUCCESS = "logout success";
    public final static String EMAIL_DOES_NOT_EXIST =  "The provided email doesnt exist.";
    public final static String EMAIL_SENT = "Email sent! please check your Email.";
    public final static String INVALID_RESET_LINK = "This is an invalid password reset link.";
    public final static String RESET_PASSWORD_SUCCESSFULLY = "Your Password has been reset. Please check your e-mail.";
    public final static String SIC_RECORD_ADDED_SUCCESSFULLY = "Sic data added successfully.";
    public final static String LOGIN_SUCCESS = "Login success";
    public final static String UPLOAD_FILE = "Uploaded the file successfully:";
    public final static String FILE_NOT_UPLOADED = "Could not upload the file: ";
    public final static String UPLOAD_CSV_FILE = "Please upload a csv file!";
    public final static String USER_NAME_REQUIRED = "User Name is required.";
    public final static String NO_DATA_FOUND = "No data found.";
    public final static String RESTORED = "Restored";
    public final static String PLEASE_CONSTACT_TO_ADMINISTRATOR = "please contact to administrator!";
    
    //mail messages
    public static final String SUBJECT =  "Your Password has been reset";
    public static final String CONTENT_FIRST = "Hi, this is your new password: ";
    public static final String CONTENT_SECOND = "\nNote: for security reason, ";
    public static final String CONTENT_THIRD = "you must change your password after logging in.";
    public static final String USERNAME_PASSWORD_WRONG = "User Name/password is wrong";
    public final static String SHARED_USER_DOES_NOT_EXIST = "Shared-to User does not exist.";

}
