package com.census.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "county_list")
@EntityListeners(AuditingEntityListener.class)
public class County {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "zipcode")
	private Integer zipcode;
	
	@Column(name = "county")
	private String county;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "short_state_name")
	private String shortStateName;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Integer getZipcode() {
		return zipcode;
	}

	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getShortStateName() {
		return shortStateName;
	}

	public void setShortStateName(String shortStateName) {
		this.shortStateName = shortStateName;
	}

}
