package com.census.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Entity
@Table(name = "share_history")
public class History {
	 	@Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private long id;
	 	
	 	@Column(name = "sharedby")
	 	private String sharedBy;
	 	
	 	@Column(name = "sharedto")
	 	private String sharedTo;
	 	
	 	@Column(name = "group_id")
	 	private Long groupId;
	 	
	 	private String status;
	 	
	 	 @CreationTimestamp
	     @Temporal(TemporalType.TIMESTAMP)
	     @Column(name = "created_at", nullable = false)
	     private Date createdAt;

	     @UpdateTimestamp
	     @Temporal(TemporalType.TIMESTAMP)
	     @Column(name = "modified_at", nullable = false)
	     private Date updatedAt;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getSharedBy() {
			return sharedBy;
		}

		public void setSharedBy(String sharedBy) {
			this.sharedBy = sharedBy;
		}

		public String getSharedTo() {
			return sharedTo;
		}

		public void setSharedTo(String sharedTo) {
			this.sharedTo = sharedTo;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public Date getCreatedAt() {
			return createdAt;
		}

		public void setCreatedAt(Date createdAt) {
			this.createdAt = createdAt;
		}

		public Date getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt(Date updatedAt) {
			this.updatedAt = updatedAt;
		}

		public Long getGroupId() {
			return groupId;
		}

		public void setGroupId(Long groupId) {
			this.groupId = groupId;
		}
}
