package com.census.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "census")
public class Census {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String uuid;
	
	@OneToMany(mappedBy = "census", fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    private Set<CensusDependent> dependentCensus;

	@Column(name = "uname")
	private String username;
	
	@Column(name = "lname")
	private String lname;
	
	@Column(name = "fname")
	private String fname;
	
	@Column(name = "gender")
	private String gender;

	@Column(name = "dob", nullable = false)
	private Date dob;

	@Column(name = "zipcode")
	private Integer zipcode;

	@Column(name = "county")
	private String county;

	@Column(name = "cobra_flag")
	private String cobraFlag;

	@Column(name = "child_flag")
	private String childFlag;

	@Column(name = "spouse_flag")
	private String spouseFlag;
	
	@Column(name = "quote")
	private String quote;
	
	@CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;


    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_at", nullable = false)
    private Date updatedAt;

	@Column(name = "status")
	private String status;
	
	@Column(name = "group_id")
	private Long groupId;
	
	@Column(name = "age")
	private String age;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	public Integer getZipcode() {
		return zipcode;
	}

	public void setZipcode(Integer zipcode) {
		this.zipcode = zipcode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getCobraFlag() {
		return cobraFlag;
	}

	public void setCobraFlag(String cobraFlag) {
		this.cobraFlag = cobraFlag;
	}

	public String getChildFlag() {
		return childFlag;
	}

	public void setChildFlag(String childFlag) {
		this.childFlag = childFlag;
	}

	public String getSpouseFlag() {
		return spouseFlag;
	}

	public void setSpouseFlag(String spouseFlag) {
		this.spouseFlag = spouseFlag;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public Set<CensusDependent> getDependentCensus() {
		return dependentCensus;
	}

	public void setDependentCensus(Set<CensusDependent> dependentCensus) {
		this.dependentCensus = dependentCensus;
	}

	
	
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Census() {
		super();
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
	
	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public Census(String username, String lname, String fname, String gender, Date dob,String age, Integer zipcode, String county,
			String cobraFlag, String childFlag, String spouseFlag, String quote , String status , Long groupId) {
		super();
		this.username = username;
		this.lname = lname;
		this.fname = fname;
		this.gender = gender;
		this.dob = dob;
		this.age = age;
		this.zipcode = zipcode;
		this.county = county;
		this.cobraFlag = cobraFlag;
		this.childFlag = childFlag;
		this.spouseFlag = spouseFlag;
		this.quote = quote;
		this.status = status;
		this.groupId = groupId;
	}
	
	
	

}
