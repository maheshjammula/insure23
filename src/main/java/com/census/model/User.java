package com.census.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * 
 * @author mahesh.jammula
 *
 */
@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
@JsonInclude(Include.NON_NULL)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(name = "uuid", unique = true ,columnDefinition = "CHAR(32)")
    private String uuid;
    
    @Column(name = "uname")
    private String username;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "enc_pwd")
    private String encPwd;
    
    @JoinColumn(name = "role_id", unique = true)
    @OneToOne(cascade = CascadeType.DETACH)
    private Role role;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "mobile")
    private long mobile;
    
    @Column(name = "acc_type")
    private String accType	;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Column(name = "pwd_reset_code")
    private String pwdResetCode;
    
    @Column(name = "status")
    private String status;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Transient
    private String password;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @Transient
    private String passwordConfirm;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;


    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_at", nullable = false)
    private Date updatedAt;
    
    @JsonIgnore
    @OneToMany(mappedBy = "user", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<UserGroup> userGroups = new ArrayList<UserGroup>();
    
    @JsonIgnore
    @OneToOne(mappedBy = "user",cascade=CascadeType.ALL)
    private UserProfile userProfile;
    
    @Transient
    private long parent_user_id;
    
    @OneToOne(optional=true)
    @JoinColumn(name="parent_user_id")
    private User parentUser;

	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUuid() {
		return uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getEncPwd() {
		return encPwd;
	}


	public void setEncPwd(String encPwd) {
		this.encPwd = encPwd;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public long getMobile() {
		return mobile;
	}


	public void setMobile(long mobile) {
		this.mobile = mobile;
	}


	public String getAccType() {
		return accType;
	}


	public void setAccType(String accType) {
		this.accType = accType;
	}


	public String getPwdResetCode() {
		return pwdResetCode;
	}


	public void setPwdResetCode(String pwdResetCode) {
		this.pwdResetCode = pwdResetCode;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getPasswordConfirm() {
		return passwordConfirm;
	}


	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public List<UserGroup> getUserGroups() {
		return userGroups;
	}


	public void setUserGroups(List<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}


	public UserProfile getUserProfile() {
		return userProfile;
	}


	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}


	public long getParent_user_id() {
		return parent_user_id;
	}


	public void setParent_user_id(long parent_user_id) {
		this.parent_user_id = parent_user_id;
	}


	public User getParentUser() {
		return parentUser;
	}


	public void setParentUser(User parentUser) {
		this.parentUser = parentUser;
	}

	
    
}
