package com.census.model;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "user_profile")
public class UserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @JoinColumn(name = "user_id", unique = true)
    @OneToOne(cascade = CascadeType.ALL)
    private User user;

    @Column(name = "fname" , columnDefinition = "NVARCHAR(400)")
    private String fname;

    @Column(name = "lname", columnDefinition = "NVARCHAR(400)")
    private String lname;

    @Column(name = "agency_name" , columnDefinition = "NVARCHAR(400)")
    private String agencyName;

    @Column(name = "co_structure", columnDefinition = "NVARCHAR(400)")
    private String coStructure;

    @Column(name = "license_no", columnDefinition = "NVARCHAR(400)")
    private String licenseNo;

    @Column(name = "license_expiry_dt", nullable = false)
    private Date licenseExpiryDt;

    @Column(name = "license_type", columnDefinition = "NVARCHAR(400)")
    private String licenseType;

    @Column(name = "address", columnDefinition = "NVARCHAR(500)")
    private String address;

    @Column(name = "city", columnDefinition = "NVARCHAR(400)")
    private String city;

    @Column(name = "state", columnDefinition = "NVARCHAR(400)")
    private String state;

    @Column(name = "zipcode", columnDefinition = "NVARCHAR(20)")
    private String zipcode;

    @Column(name = "status", columnDefinition = "NVARCHAR(10)")
    private String status;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "modified_at", nullable = false)
    private Date updatedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() { return user; }

    public void setUser(User user) { this.user = user; }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getCoStructure() {
        return coStructure;
    }

    public void setCoStructure(String coStructure) {
        this.coStructure = coStructure;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public Date getLicenseExpiryDt() { return licenseExpiryDt; }

    public void setLicenseExpiryDt(Date licenseExpiryDt) { this.licenseExpiryDt = licenseExpiryDt; }

    public String getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(String licenseType) {
        this.licenseType = licenseType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "id=" + id +
                ", user=" + user +
                ", fname='" + fname + '\'' +
                ", lname='" + lname + '\'' +
                ", agencyName='" + agencyName + '\'' +
                ", coStructure='" + coStructure + '\'' +
                ", licenseNo='" + licenseNo + '\'' +
                ", licenseExpiryDt='" + licenseExpiryDt + '\'' +
                ", licenseType='" + licenseType + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", status='" + status + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
